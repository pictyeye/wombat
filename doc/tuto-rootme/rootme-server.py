#!/usr/bin/python3

"""rootme is a simplified variant of the TCPServer present in
src/tcp. It consists in a TCP server accepting connections. When a
client connects, the server displays the used RSA public key (n, e)
and a encrpyted message containing a secret string (the challenge).

The client can then submit encrypted values, and observe the behaviour
of the server.

All integers values are represented by hexadecimal strings.

"""

import binascii
import codecs
import sys
import socket
from threading import Thread, Event
import argparse
from wombat.SimpleOracles import (
    TTT_Oracle,
    FTT_Oracle,
    TFT_Oracle,
    FFT_Oracle,
    StubFromOracle,
    ExplicitOracleBehaviour,
)


def handle_client(conn, stub):
    """This function handles a client throughout its connection. Its
    arguments are:

        - conn, the open connection,
        - stub, the underlying connector to the implementation we

    First, the function displays n, e and the challenge. Then for each
    encrypted message submitted by the peer, we give it to the oracle
    and produce the corresponding behaviour to be returned to the
    client.

    """

    try:
        challenge = codecs.encode(stub.get_challenge(), "hex")
        conn.send(b"%x\n%x\n%s\n\n" % (stub.n(), stub.e(), challenge))
        msg = conn.recv(16384).strip()
        while msg != b"":
            try:
                conn.send(b"%s\n" % stub.decrypt(codecs.decode(msg, "hex")))
            except binascii.Error:
                conn.send(b"Invalid input\n")
            msg = conn.recv(16384).strip()
    except OSError:
        pass
    conn.close()


def launch_tcp_server(stub, address, timeout=None):
    """This function launches a TCP server and lets handle_client handle
    each new connection in a separate threas. Its arguments are:

        - stub, the underlying connector to the implementation we
        - address, the bind address
        - timeout, a maximum delay to wait for each client socket
    """

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(address)
    server_socket.listen(5)

    while True:
        s, client_address = server_socket.accept()
        if timeout is not None:
            s.settimeout(timeout)
        t = Thread(target=handle_client, args=(s, stub))
        t.start()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Starts a rootme-like TCP server exposing a vulnerable RSA implementation"
    )
    parser.add_argument(
        "--address",
        action="store",
        type=str,
        dest="listen_address",
        default="127.0.0.1",
        help="set the address to listen to (default is 127.0.0.1)",
    )
    parser.add_argument(
        "--port",
        action="store",
        type=int,
        dest="listen_port",
        default=12345,
        help="set the port to use (default is 12345)",
    )
    parser.add_argument(
        "--timeout",
        action="store",
        type=int,
        dest="timeout",
        default=None,
        help="maximum delay before closing an inactive socket (default is None)",
    )
    parser.add_argument(
        "-m",
        action="store",
        type=str,
        dest="message",
        default="This is a TRUE secret. Can you find it?",
        help="define the secret message used for the challenge",
    )

    args = parser.parse_args()

    oracle = TTT_Oracle()
    oracle.challenge_plaintext = args.message.encode()
    stub = StubFromOracle(oracle, ExplicitOracleBehaviour(b"1", b"0"))

    launch_tcp_server(
        stub, (args.listen_address, args.listen_port), timeout=args.timeout
    )
