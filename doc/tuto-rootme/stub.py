"""Simple stub to connect Wombat to the rootme server"""

import socket
import codecs
from wombat.WTypes import WStub


class Stub(WStub):
    def __init__(self, address):
        WStub.__init__(self)
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect(address)

        public_infos = self.socket.recv(16384).split(b"\n")
        n = int(public_infos[0], 16)
        e = int(public_infos[1], 16)
        self.set_public_key(n, e)

        self.challenge = codecs.decode(public_infos[2], "hex")

    def do_decrypt(self, c):
        self.socket.send(codecs.encode(c, "hex"))
        return self.socket.recv(16384).strip()
