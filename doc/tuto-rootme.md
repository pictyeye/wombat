# How to use Wombat to exploit a rootme-like server


## Setup

For this tutorial, we will need the wombat Docker image, which can be
built using the following command (working directory is assumed to be
your working copy of the wombat project):

    $ make -C docker wombat

You can also retrieve it from the Docker Hub

    $ docker pull pictyeye/wombat

In this case, you will have to use pictyeye/wombat instead of wombat
in the remaining of the tutorial.


## Running the vulnerable server

First we create a network to connect our containers:

    docker network create wombat

Let us run the wombat container, and launch the vulnerable server:

    $ docker run --name rootme-server -ti --rm \
                 -p 12345:12345 --network wombat wombat

    (docker)# cd doc/tuto-rootme
    (docker)# ./rootme-server.py --address 0.0.0.0 \
                                 --port 12345 \
                                 -m "This is a SECRET!"


## Connecting to the server

Let us create a second Docker to interact with the vulnerable server.

    $ docker run --name wombat -ti --rm --network wombat wombat

    (docker)# apt update && apt install -y netcat
    (docker)# nc rootme-server 12345
    b6d3dc135f193e9ee3e215856cce221d3c98ed6d1d6ce81ca9...15
    10001
    4b316a105693a16cabbc006374e98c1c7eed427707897de850...6c

Once you contact the server, you receive the public key (the modulus n
is the first line, the public exponent e is the second one) and a
challenge, which consists of the message we gave earlier, encrypted
using the public key.

If we send back the challenge to the server, we get back 1. If we send
an altered version of the challenge, you will most certainly get
back 0. So, the server seems to behave differently when fed with
different ciphertext. There might be a Bleichenbacher oracle awaiting
to be identified and exploited...


## Writing a stub to connect to the server

To interact with the server, let us write a simple stub. We will have
to create a WStub subclass, as shown in tuto-rootme/stub.py.

First, we need to import some obvious modules

    import socket
    import codecs
    from wombat.WTypes import WStub

Then, the constructor will open the connection to the server we want
to work with, and read the information from the socket: n, e and the
challenge.

    class Stub(WStub):
        def __init__(self, address):
            WStub.__init__(self)
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.connect(address)

            public_infos = self.socket.recv(16384).split(b"\n")
            n = int(public_infos[0], 16)
            e = int(public_infos[1], 16)
            self.set_public_key(n, e)

            self.challenge = codecs.decode(public_infos[2], "hex")

Finally, we need to tell how to submit a encrypted message c, by
writing it on the socket, and by reading the result back.

        def do_decrypt(self, c):
            self.socket.send(codecs.encode(c, "hex"))
            return self.socket.recv(16384).strip()

And that is it! We now can simply interact with the server and use the
algorithms provided by wombat.


## Identification of the oracle

To identify the oracle, the simplest way to do it is to use a Python
interpreter:

    (docker)# cd doc/tuto-rootme
    (docker)# python
    >>> import stub
    >>> from wombat.SimpleOracles import OracleFromStub
    >>> from wombat.DecryptionAttack import DecryptionAttack, AttackAlgo
    >>> from wombat.OracleAssessment import identify_oracle
    >>>
    >>> stub = stub.Stub(("rootme-server", 12345))
    >>> oracle_type, true_signals, false_signals = identify_oracle(stub)

If all goes well, oracle_type should contain "TTT", indicating that
the server is vulnerable, and that it exhibits a TTT oracle (as per
the classification discussed in Bardou et al.). You should also learn
that the server returns 1 in case we sent a ciphertext corresponding
to a valid padding, and 0 in the other cases.


## Exploitation

If you want to go all the way, you can then, still in your
interpreter, try and attack the server. First, you must build an
oracle with the intelligence gathered:

    >>> o = OracleFromStub(stub, true_signals, false_signals)

Then, you can launch the attack and extract the message from the
challenge:

    >>> attack = DecryptionAttack(o, algo=AttackAlgo.OptimisedAttack)
    >>> msg = attack.run(o.get_challenge())
    >>> msg
    b'This is a SECRET!'

If all goes well, msg should contain the message you initially gave to
the server command line. Depending on your luck, this might take a
long time.