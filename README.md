# Wombat: one more Bleichenbacher attacks toolbox


Wombat is a toolkit to identify, assess and exploit padding oracles in
RSA PKCS#1 v1.5 implementations (following the attack devised by
Daniel Bleichenbacher in 1998). You should use it in a lawful manner,
which includes security challenges, educational purpose or legitimate
security audits.

Wombat is compatible with Python3 and is not tested anymore against a
Python2 environment.


## Quick tests with the Dockerfile

To start playing with Wombat, you can download the Docker image on
hub.docker.io, using the following command:

    % docker pull pictyeye/wombat

Then you can run a container and checks that it works:

    % docker run -ti --rm pictyeye/wombat
    % make check


## Local environment

If you want to code, it might be better to get comfortable with a
local version.  To prepare your environment to compile wombat, the
simplest way is to use virtualenv:

    % python -m virtualenv -p python3 venv
    % source venv/bin/activate
    (venv)% pip install -r requirements.txt

You can locally rebuild the Docker image:

    % cd docker
    % docker-scripts.sh mk_all
    % docker run -ti --rm wombat

Alternatively, you could install all the dependencies using your
package manager, but we do not guarantee that this will work. You can
still try doing

    % apt install python3-cryptography python3-pytest python3-pytest-pylint python3-pytest-cov python3-scapy


Once you are settled with wombat, you can check that the project works
by calling

    % make check




## Documentation

The documentation is a work in progress, but you should find tutorials
in the doc directory.


The project is developped on
[GitLab](https://gitlab.com/pictyeye/wombat), and
bugs/issues/contributions should be reported there. We are also
 maintaining a [website](https://wombat.ebfe.fr).
