.PHONY: check coverage clean docker

all:
	make -C doc
	make -C docker

check:
	make -C src check

coverage:
	make -C src coverage

clean:
	make -C src clean
	make -C doc clean
	make -C docker clean
