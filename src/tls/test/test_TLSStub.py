import os
import socket
import ssl
import time
import random
from datetime import datetime, timedelta
import tempfile
from threading import Thread, Event
from cryptography.hazmat.backends import default_backend
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
import pytest
from wombat.OracleAssessment import identify_oracle
from tls.TLSStub import TLSStub


def mk_tmp_file(tmpdir, name, content):
    fn = os.path.join(tmpdir, name)
    with open(fn, "wb") as f:
        f.write(content)
        f.close()
        return fn


def mk_cert_and_key(hostname, key_size):
    key = rsa.generate_private_key(
        public_exponent=65537, key_size=key_size, backend=default_backend()
    )

    name = x509.Name([x509.NameAttribute(NameOID.COMMON_NAME, hostname)])
    basic_contraints = x509.BasicConstraints(ca=True, path_length=0)
    now = datetime.utcnow()
    cert = (
        x509.CertificateBuilder()
        .serial_number(random.randint(10, 1000))
        .not_valid_before(now)
        .not_valid_after(now + timedelta(days=1))
        .subject_name(name)
        .issuer_name(name)
        .public_key(key.public_key())
        .add_extension(basic_contraints, False)
        .sign(key, hashes.SHA256(), default_backend())
    )
    cert_content = cert.public_bytes(encoding=serialization.Encoding.PEM)
    key_content = key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption(),
    )
    return cert_content, key_content


def launch_tls_server(address, stopper, timeout=None):
    ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ctx.minimum_version = ssl.TLSVersion.TLSv1
    ctx.maximum_version = ssl.TLSVersion.TLSv1_2
    ctx.check_hostname = False
    ctx.set_ciphers('kRSA')
    ctx.verify_mode = ssl.CERT_NONE

    cert, key = mk_cert_and_key("localhost", 2048)

    tmpdir = tempfile.mkdtemp(prefix="wombat-tmp")
    cert_fn = mk_tmp_file(tmpdir, "cert.pem", cert)
    key_fn = mk_tmp_file(tmpdir, "key.pem", key)

    ctx.load_cert_chain(cert_fn, key_fn)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    if timeout is not None:
        sock.settimeout(timeout)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(address)
    sock.listen(5)
    ssock = ctx.wrap_socket(sock, server_side=True)
    stopper.clear()

    while not stopper.is_set():
        try:
            conn, _addr = ssock.accept()
            conn.close()
        except OSError:
            pass


@pytest.fixture
def make_tls_server():
    def _make_tls_server():
        port = random.randint(10000, 20000)
        stopper = Event()
        t = Thread(
            target=launch_tls_server,
            args=(("127.0.0.1", port), stopper),
            kwargs={"timeout": 2.0},
        )
        stopper.set()
        t.start()
        while stopper.is_set():
            time.sleep(0.1)
        return t, port, stopper

    return _make_tls_server


def test_public_key(make_tls_server):
    ssl_server, port, stopper = make_tls_server()
    try:
        stub = TLSStub("127.0.0.1", port)
        assert stub.e() == 65537
    finally:
        stopper.set()
        ssl_server.join()


def test_prepare_decrypt(make_tls_server):
    ssl_server, port, stopper = make_tls_server()
    try:
        stub = TLSStub("127.0.0.1", port)

        # prepare_decrypt will first get the public key,
        # then submit a ClientHello and get the certificate a second time
        # The code checks that the certificate received is the same both time
        stub.decrypt(stub.get_challenge())
    finally:
        stopper.set()
        ssl_server.join()


def test_identification_failure(make_tls_server):
    ssl_server, port, stopper = make_tls_server()
    try:
        stub = TLSStub("127.0.0.1", port)
        oracle_type, _, _ = identify_oracle(stub)
        assert oracle_type == "Unknown"
    finally:
        stopper.set()
        ssl_server.join()
