"""TLSStub allows to interact with a TLS server to then submit
encrypted messages."""

from struct import pack, unpack
import socket
import ssl
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from scapy.all import NoPayload, raw
from scapy.layers.tls.all import (
    TLS,
    TLSClientHello,
    TLSChangeCipherSpec,
    TLSClientKeyExchange,
)
from wombat.WTypes import WStub


class TLSHandshakeFailed(Exception):
    """Exception raised when a problem arises during the early stages of
    the connection (in particular, before the ClientKeyExchange is
    submitted). Indeed errors that happen later can be interpreted as
    an interesting behaviour from the server."""


class UnexpectedCertificate(Exception):
    """The certificate presented by the server has changed since the
    beginning of the interaction. Actually, this exception is raised
    in case of a public key mismatch."""


def truncated_records(s):
    """Helper used to tell whether s contains only entire records."""

    if len(s) < 5:
        return True
    (_content_type, _version, content_length) = unpack("!BHH", s[0:5])
    if len(s) < 5 + content_length:
        return True
    if len(s) == 5 + content_length:
        return False
    return truncated_records(s[5 + content_length :])


class TLSStub(WStub):
    """TLSStub allows to interact with a TLS server. The constructor only
    records the given parameters. The public key will be retrieved the
    first time get_public_key() is called. A connection will then be
    established to obtain the RSA public key. The stub can then be
    used to submit encrypted messages."""

    def __init__(self, host, port, timeout=3):
        WStub.__init__(self)
        self.challenge_plaintext = b"\x03\x03" * 24
        self.address = (host, port)
        self.socket = None
        self.timeout = timeout
        self.second_server_flight = None

    def get_public_key(self):
        """Retrieve the public key by establishing a TLS session the first
        time it is called. The parameters are chosen to avoid TLS 1.3
        and to force RSA key exchange."""

        if self.public_key_object is None:
            ctx = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
            ctx.minimum_version = ssl.TLSVersion.TLSv1
            ctx.maximum_version = ssl.TLSVersion.TLSv1_2
            ctx.check_hostname = False
            ctx.verify_mode = ssl.CERT_NONE
            ctx.set_ciphers("kRSA")
            raw_socket = socket.socket()
            s = ctx.wrap_socket(raw_socket)
            s.connect(self.address)
            cert_raw = s.getpeercert(binary_form=True)
            cert = x509.load_der_x509_certificate(cert_raw, default_backend())
            s.close()
            self.public_key_object = cert.public_key()
        return self.public_key_object

    def check_certificate(self, cert_msg):
        """Simple helper to check that the public key encountered is still the
        same as the one retrieved earlier."""
        server_cert = cert_msg.certs[0][1]
        if (
            server_cert.pubKey.pubkey.public_numbers()
            != self.get_public_key().public_numbers()
        ):
            raise UnexpectedCertificate

    def prepare_decrypt(self):
        """This method handles the preliminary steps required before
        submitting an encrypted message. Indeed, we need to send a
        ClientHello and to receive the first flight of server
        messages.

        We move this code here to handle properly the time spent to
        decrypt the message. We also treat TLS errros differently when
        they happen here."""

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(self.timeout)
        self.socket.connect(self.address)
        client_hello = TLS(msg=[TLSClientHello(ciphers=[0x35], version=0x0301)])
        self.socket.send(raw(client_hello))

        for _i in range(5):
            try:
                raw_server_packets = b""
                while truncated_records(raw_server_packets):
                    data = self.socket.recv(16384)
                    if not data:
                        raise TLSHandshakeFailed
                    raw_server_packets += data
                server_packets = TLS(raw_server_packets)
            except OSError as e:
                raise TLSHandshakeFailed from e

            while not isinstance(server_packets, NoPayload):
                if server_packets.type == 0x15 and server_packets.msg[-1].level == 1:
                    raise TLSHandshakeFailed
                if server_packets.type == 0x16:
                    for m in server_packets.msg:
                        if m.msgtype == 0xB:
                            self.check_certificate(m)
                        if m.msgtype == 0xE:
                            return
                server_packets = server_packets.payload

    def do_decrypt(self, c):
        """This method corresponds to the real submission of an encrypted
        message. We send a ClientKeyExchange encapsulating the target
        message, and then we read what the server has to answer."""

        c = pack("!H", len(c)) + c
        client_key_exchange = TLS(msg=TLSClientKeyExchange(exchkeys=c))
        self.socket.send(raw(client_key_exchange))
        try:
            change_cipher_spec = TLS(msg=TLSChangeCipherSpec())
            self.socket.send(raw(change_cipher_spec))
            raw_client_finished = b"\x16\x03\x03\x00\x40" + (b" " * 64)
            self.socket.send(raw_client_finished)
        except OSError:
            pass
        try:
            self.second_server_flight = self.socket.recv(8192)
        except OSError:
            self.second_server_flight = None

    def cleanup_decrypt(self):
        """When we arrive in cleanup_decrypt, the encrypted message has been
        submitted and we recorded the answer from the server. It is
        now time to properly close the connection and return the
        answer from the server."""

        alert_descr = None
        packet_type = None
        if self.second_server_flight is not None:
            try:
                packets = TLS(self.second_server_flight)
                packet_type = packets.type
                if packets.type == 0x15:
                    alert_descr = packets.msg[0].descr
            except OSError:
                pass
        self.socket.close()
        return packet_type, alert_descr
