#!/usr/bin/env python3

import socket
import ssl
import argparse
from wombat.OracleAssessment import identify_oracle
from tls.TLSStub import TLSStub, TLSHandshakeFailed


def resolve_domain(domain, port):
    try:
        return list(
            map(
                lambda x: x[4],
                socket.getaddrinfo(
                    domain, port, family=socket.AF_INET, proto=socket.IPPROTO_TCP
                )[: args.max_ips],
            )
        )
    # pylint: disable=broad-except
    except Exception:
        return []


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Test the presence of Bleichenbacher oracles in TLS servers."
    )
    parser.add_argument(
        "-n",
        action="store",
        type=int,
        dest="n_tests",
        default=3,
        help="number of tests per stimulus to identify the oracle (default is 3)",
    )
    parser.add_argument(
        "--max-ips",
        action="store",
        type=int,
        dest="max_ips",
        default=3,
        help="maximum number of IPs tested for a given domain (default is 3)",
    )
    parser.add_argument(
        "-p",
        action="store",
        type=int,
        dest="port",
        default=443,
        help="the TCP port to use for the connections (default is 443)",
    )
    parser.add_argument(
        "servers", metavar="server", nargs="+", help="the servers to test"
    )

    args = parser.parse_args()

    for server in args.servers:
        addresses = resolve_domain(server, args.port)
        if len(addresses) <= 0:
            print(f"{server}: Unable to resolve the domain name")

        for (address, port) in addresses:
            stub = TLSStub(address, port)

            # pylint: disable=broad-except
            try:
                pk = stub.get_public_key()
            except ssl.SSLError:
                print(f"{server}[{address}]: No oracle found (TLS not supported?)")
                continue
            except TLSHandshakeFailed:
                print(f"{server}[{address}]: No oracle found (RSA not supported?)")
                continue
            except Exception as e:
                print(f'{server}[{address}]: No oracle found (Unkown error "{e}")')
                continue

            oracle_type, true_signal, false_signal = identify_oracle(stub, args.n_tests)
            if oracle_type != "Unknown":
                print(
                    f"{server}[{address}]: Explicit {oracle_type} oracle found "
                    + f"(true-signal={true_signal}, false_signal={false_signal})"
                )
            else:
                print(f"{server}[{address}]: No oracle found")
