import subprocess
from tempfile import NamedTemporaryFile
from cryptography.hazmat.primitives import serialization
import xmlsec
from lxml import etree
from wombat.WTypes import WStub
from xmlenc import XMLEnc, XMLDec


class XMLStub(WStub):
    """Example of stub, from our own xmlEnc implementation"""

    def __init__(self, public_key, private_key):
        WStub.__init__(self)
        self.public_key_object = public_key
        self.private_key_object = private_key

    def get_challenge(self):
        message = b"IV and 3DES key"
        return self.encrypt(message)

    def do_decrypt(self, c):
        try:
            # encrypted_xml with altered key sent to the oracle
            encrypted_xml = str(
                XMLEnc.create_encrypted_xml_for_stub(c, b"lambda message"), "utf-8"
            )
            return XMLDec.xml_decryption_str_input(
                encrypted_xml, self.private_key_object
            )
        # pylint: disable=broad-except
        except Exception as e:
            signal = str(e)
            if signal == "Encryption/decryption failed.":
                return "Decryption fail"
            return signal[:16]


class Xmlsec1Stub(WStub):
    """Debian xmlsec1 stub"""

    def __init__(self, public_key, private_key):
        WStub.__init__(self)
        self.public_key_object = public_key
        self.private_key_object = private_key

        # serialized key
        self.key_pem = self.private_key_object.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption(),
        )

    def get_challenge(self):
        message = b"IV and 3DES key"
        return self.encrypt(message)

    def do_decrypt(self, c):
        # encrypted_xml with altered key sent to the oracle
        encrypted_xml = XMLEnc.create_encrypted_xml_for_stub(c, b"lambda message")

        # capture of oracle's messages
        with NamedTemporaryFile() as key:
            key.write(self.key_pem)
            key.flush()
            with NamedTemporaryFile() as xml_doc:
                xml_doc.write(encrypted_xml)
                xml_doc.flush()
                code = subprocess.run(
                    ["xmlsec1", "--decrypt", "--privkey-pem", key.name, xml_doc.name],
                    check=False,
                    capture_output=True,
                )
        return code.stderr.split(b":")[0]


class XmlsecStub(WStub):
    """xmlsec stub (xmlsec Python package)"""

    def __init__(self, public_key, private_key):
        WStub.__init__(self)
        self.public_key_object = public_key
        self.private_key_object = private_key

    def get_challenge(self):
        message = b"IV and 3DES key"
        return self.encrypt(message)

    def do_decrypt(self, c):
        try:
            # encrypted_xml with altered key sent to the oracle
            encrypted_xml = XMLEnc.create_encrypted_xml_for_stub(c, b"lambda message")

            # Decryption
            manager = xmlsec.KeysManager()
            key = xmlsec.Key.from_memory(
                self.private_key_object, xmlsec.constants.KeyDataFormatPem
            )
            # key = xmlsec.Key.from_file(
            #     self.rsa_key_file, xmlsec.constants.KeyDataFormatPem
            # )
            # xmlsec.enable_debug_trace(True)
            manager.add_key(key)
            enc_ctx = xmlsec.EncryptionContext(manager)
            root = etree.XML(encrypted_xml)
            tag_root = etree.QName(root).localname
            if tag_root != "EncryptedData":
                enc_data = xmlsec.tree.find_child(
                    root, "EncryptedData", xmlsec.constants.EncNs
                )
            else:
                enc_data = root
            decrypted = enc_ctx.decrypt(enc_data)
            return decrypted
        # pylint: disable=broad-except
        except Exception as e:
            return str(e)
