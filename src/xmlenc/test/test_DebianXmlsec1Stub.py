import pytest
from cryptography.hazmat.primitives.asymmetric.rsa import generate_private_key
from cryptography.hazmat.backends import default_backend
from xmlenc import XMLStub
from wombat.OracleAssessment import identify_oracle, evaluate_oracle
from wombat.SimpleOracles import OracleFromStub
from wombat.DecryptionAttack import DecryptionAttack, AttackAlgo


@pytest.fixture
def private_key():
    return generate_private_key(
        public_exponent=65537, key_size=2048, backend=default_backend()
    )


@pytest.fixture
def public_key(private_key):
    return private_key.public_key()


def test_identify_oracle(public_key, private_key):
    """Stub for Debian xmlsec1 decryptor"""
    stub = XMLStub.Xmlsec1Stub(public_key, private_key)
    assert stub.e() == 65537
    oracle_type, true_signals, false_signals = identify_oracle(stub, n=50)
    assert oracle_type == "FFT"
    assert true_signals == {
        b"func=xmlSecKeyDataBinaryValueBinRead",
        b"func=xmlSecOpenSSLEvpBlockCipherCtxUpdateBlock",
    }
    assert false_signals == {b"func=xmlSecOpenSSLRsaPkcs1Process"}
    oracle = OracleFromStub(stub, true_signals, false_signals)
    oracle.treat_unknown_behaviour_as_false_signal = False
    answers = evaluate_oracle(oracle, n=50)
    assert answers == [50, 0, 0, 0, 0]


@pytest.mark.slow
def test_attack(public_key, private_key):
    """Test the attack on xmlsec1"""
    stub = XMLStub.Xmlsec1Stub(public_key, private_key)
    challenge = stub.get_challenge()
    _, true_signals, false_signals = identify_oracle(stub, n=50)
    oracle = OracleFromStub(stub, true_signals, false_signals)
    oracle.treat_unknown_behaviour_as_false_signal = False
    attack = DecryptionAttack(oracle, algo=AttackAlgo.OptimisedAttack)
    msg = attack.run(challenge)
    assert msg == b"IV and 3DES key"
