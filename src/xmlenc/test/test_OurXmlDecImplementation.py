import pytest
from cryptography.hazmat.primitives.asymmetric.rsa import generate_private_key
from cryptography.hazmat.backends import default_backend
from xmlenc import XMLEnc, XMLDec, XMLStub
from wombat.OracleAssessment import identify_oracle, evaluate_oracle
from wombat.SimpleOracles import OracleFromStub
from wombat.DecryptionAttack import DecryptionAttack, AttackAlgo


@pytest.fixture
def private_key():
    return generate_private_key(
        public_exponent=65537, key_size=2048, backend=default_backend()
    )


@pytest.fixture
def public_key(private_key):
    return private_key.public_key()


def test_xml_encryption_decryption_str(public_key, private_key):
    """Encryption->decryption test, input is a string"""
    message = "message"
    encrypted_xml = XMLEnc.xml_encryption_str_input(message, public_key)
    decrypted_xml = XMLDec.xml_decryption_str_input(encrypted_xml, private_key)
    decrypted_xml = str(decrypted_xml, "utf-8")
    assert message == decrypted_xml, "incorrect xml encryption/decryption"


def test_identify_oracle(public_key, private_key):
    """Stub with our own XML decryptor"""
    stub = XMLStub.XMLStub(public_key, private_key)
    assert stub.e() == 65537
    oracle_type, true_signals, false_signals = identify_oracle(stub)
    assert oracle_type == "FFT"
    assert "Invalid" in list(true_signals)[0]
    assert "Decryption fail" in list(false_signals)[0]
    # Oracle sometimes returns "index out of range"
    oracle = OracleFromStub(stub, true_signals, false_signals)
    oracle.treat_unknown_behaviour_as_false_signal = True
    answers = evaluate_oracle(oracle, n=1)
    assert answers == [1, 0, 0, 0, 0]


@pytest.mark.slow
def test_attack(public_key, private_key):
    """Test the attack on our xml_oracle from our XMLStub"""
    stub = XMLStub.XMLStub(public_key, private_key)
    challenge = stub.get_challenge()
    _, true_signals, false_signals = identify_oracle(stub)
    oracle = OracleFromStub(stub, true_signals, false_signals)
    oracle.treat_unknown_behaviour_as_false_signal = True
    attack = DecryptionAttack(oracle, algo=AttackAlgo.OptimisedAttack)
    msg = attack.run(challenge)
    assert msg == b"IV and 3DES key"
