import pytest
from cryptography.hazmat.primitives.asymmetric.rsa import generate_private_key
from cryptography.hazmat.backends import default_backend
from xmlenc import XMLStub
from wombat.OracleAssessment import identify_oracle


@pytest.fixture
def private_key():
    return generate_private_key(
        public_exponent=65537, key_size=2048, backend=default_backend()
    )


@pytest.fixture
def public_key(private_key):
    return private_key.public_key()


def test_xmlsec_python_stub(public_key, private_key):
    """Stub with python xmlsec decryptor.
    TODO: try to find a better oracle with the xmlsec.enable_debug_trace(True) function."""
    stub = XMLStub.XmlsecStub(public_key, private_key)
    assert stub.e() == 65537
    oracle_type, true_signals, false_signals = identify_oracle(stub, n=10)
    assert oracle_type == "Unknown"
    assert true_signals == false_signals == []
