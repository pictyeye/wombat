import base64
import re
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes


######### BLOCK DETECTION ##########
def get_encrypted_data_block(xml_str):
    """WE EXTRACT XML BLOCKS, NOT THE KEY OR DATA ALONE
    Detect, extract and return the whole encrypted XML block of an XML file
    (An XML file can contain non encrypted portions)
    Begins with : <EncryptedData>...</EncryptedData>"""
    found_beginning = False
    found_end = False
    encrypted_data_block_str = ""
    for line in xml_str.splitlines():
        if line.find("<EncryptedData") != -1:
            found_beginning = True
        if found_beginning and not found_end:
            encrypted_data_block_str += line + "\n"
        if line.find("</EncryptedData>") != -1:
            found_end = True
    return encrypted_data_block_str


def get_cipher_key_block(encrypted_data_block):
    """From the encrypted block, extract and return the XML block
    containing the symmetric key and its infos
    Begins with : <EncryptedKey>..b64key and infos about the asymmetric
    encryption algorithm..</EncryptedKey>"""
    found_beginning = False
    found_end = False
    cipher_key_block_str = ""
    for line in encrypted_data_block.splitlines():
        if line.find("<EncryptedKey") != -1:
            found_beginning = True
        if found_beginning and not found_end:
            cipher_key_block_str += line + "\n"
        if line.find("</EncryptedKey>") != -1:
            found_end = True
    return cipher_key_block_str


def get_data_block(encrypted_data_block):
    """From the encrypted block, extract and return the XML block
    containing the encrypted data
    Begins with : <CipherData><CipherValue>..b64msg..</CipherValue></CipherData>"""
    found_beginning = False
    found_end = False
    data_block_str = ""
    num_beginning_line = 0
    for num, line in enumerate(encrypted_data_block.splitlines()):
        if line.find("</KeyInfo>") != -1:
            num_beginning_line = num
    for num, line in enumerate(encrypted_data_block.splitlines()):
        if num > num_beginning_line:
            if line.find("<CipherData>") != -1:
                found_beginning = True
            if found_beginning and not found_end:
                data_block_str += line + "\n"
            if line.find("</CipherData>") != -1:
                found_end = True
    return data_block_str


############ END BLOCK DETECTION #############


############ EXTRACTION OF THE CIPHER VALUES ###########
def get_cipher_value_b64(block):
    """Return what is between <CipherValue>...</CipherValue> (useful to get
    cipher_key and cipher_data) in the block"""
    found_beginning = False
    found_end = False
    cipher_value_b64_str = ""
    for line in block.splitlines():
        if line.find("<CipherValue>") != -1:
            found_beginning = True
        if found_beginning and not found_end:
            cipher_value_b64_str += re.sub("<.*?>", "", line).strip()
        if line.find("</CipherValue>") != -1:
            found_end = True
    return cipher_value_b64_str


def get_cipher_value_key_data(xml_str):
    """Extract and return both b64key and b64msg"""
    encrypted_block = get_encrypted_data_block(xml_str)
    cipher_key_block = get_cipher_key_block(encrypted_block)  # Key block
    cipher_data_block = get_data_block(encrypted_block)  # Message block

    # Extraction of the b64 cipher values
    b64_key = get_cipher_value_b64(cipher_key_block)
    b64_msg = get_cipher_value_b64(cipher_data_block)

    return b64_key, b64_msg


########## END CIPHER VALUE EXTRACTION ###########


def get_private_rsa_key(private_rsa_key_file):
    """load a private RSA key from a key.pem file"""
    private_key = serialization.load_pem_private_key(
        private_rsa_key_file.read(), password=None, backend=default_backend()
    )
    return private_key


def rsa_decryption_with_pkcs1v15(rsa_key, ciphered_symmetric_key):
    """RSA decryption with PKCS1v15 padding, to get the symmetric key"""
    symmetric_key = rsa_key.decrypt(ciphered_symmetric_key, padding.PKCS1v15())
    return symmetric_key


def triple_des_decryption(ciphered_msg, symmetric_key):
    """Decrypt the message encrypted by the symmetric algorithm
    (for now we will use 3DES algorithm, but later we could choose
    depending on the info given in the encrypted XML file)"""
    iv = ciphered_msg[:8]
    true_ciphered_msg = ciphered_msg[8:]
    cipher = Cipher(
        algorithms.TripleDES(symmetric_key), modes.CBC(iv), backend=default_backend()
    )
    decrypter = cipher.decryptor()
    decoded_msg = decrypter.update(true_ciphered_msg)
    return decoded_msg


def remove_extra_padding(decoded_data):
    """Erase padding resulting from 3DES encryption (PKCS5 padding)"""
    nb = decoded_data[-1]
    return decoded_data[:-nb]


def msg_decryption(ciphered_msg, ciphered_key, rsa_key):
    """Decrypt b64_msg (not the whole xmlEncrypted file)"""
    hex_msg = base64.b64decode(ciphered_msg)
    hex_key = base64.b64decode(ciphered_key)
    symmetric_key = rsa_decryption_with_pkcs1v15(rsa_key, hex_key)
    decoded_msg = triple_des_decryption(hex_msg, symmetric_key)
    decoded_msg = remove_extra_padding(decoded_msg)
    return decoded_msg


def xml_decryption_str_input(xml_str, private_rsa_key):
    """Decrypt an XML encrypted string"""
    ciphered_key, ciphered_msg = get_cipher_value_key_data(xml_str)
    decoded_msg = msg_decryption(ciphered_msg, ciphered_key, private_rsa_key)
    return decoded_msg


def xml_decryption_file_input(xml_file, rsa_key_file):
    """Decrypt an XML encrypted file"""
    with open(xml_file, encoding="utf8") as encrypted_xml_file:
        xml_str = encrypted_xml_file.read()
    with open(rsa_key_file, "rb") as private_key_file:
        rsa_key = get_private_rsa_key(private_key_file)
    ciphered_key, ciphered_msg = get_cipher_value_key_data(xml_str)
    decoded_data = msg_decryption(ciphered_msg, ciphered_key, rsa_key)
    return decoded_data


def create_decrypted_file(file_name, decoded_data):
    """Write a string into a file"""
    with open(file_name, "w", encoding="utf8") as decrypted_file:
        decrypted_file.write(str(decoded_data, "utf-8"))
