import base64
import os
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import padding

XML_URI = {
    "enc": "http://www.w3.org/2001/04/xmlenc#",
    "dsig": "http://www.w3.org/2000/09/xmldsig#",
}

PARAMETER_TYPE_URI = {
    "element": "http://www.w3.org/2001/04/xmlenc#Element",
    "content": "http://www.w3.org/2001/04/xmlenc#Content",
}

BLOCK_ENCRYPTION_ALGORITHM_URI = {
    "tripledes": "http://www.w3.org/2001/04/xmlenc#tripledes-cbc",
    "aes-128": "http://www.w3.org/2001/04/xmlenc#aes128-cbc",
    "aes-256": "http://www.w3.org/2001/04/xmlenc#aes256-cbc",
    "aes128-gcm": "http://www.w3.org/2009/xmlenc11#aes128-gcm",
    "aes-192": "http://www.w3.org/2001/04/xmlenc#aes192-cbc",
    "aes192-gcm": "http://www.w3.org/2009/xmlenc11#aes192-gcm",
    "aes256-gcm": "http://www.w3.org/2009/xmlenc11#aes256-gcm",
}

KEY_TRANSPORT_ALGORITHM_URI = {
    "rsa-oaep-mgf1p": "http://www.w3.org/2001/04/xmlenc#rsa-oaep-mgf1p",
    "rsa-oaep": "http://www.w3.org/2001/04/xmlenc#rsa-oaep",
    "rsa-1_5": "http://www.w3.org/2001/04/xmlenc#rsa-1_5",
}


def get_encrypted_xml_string(
    cipher_key,
    cipher_data,
    parameter_type="element",
    block_encryption_algorithm="tripledes",
    key_transport_algorithm="rsa-1_5",
):
    """Return the encrypted XML to string format"""
    encrypted_xml_string = ""
    encrypted_xml_string += '<?xml version="1.0"?>\n'
    encrypted_xml_string += (
        '<EncryptedData xmlns="'
        + XML_URI["enc"]
        + '" Type="'
        + PARAMETER_TYPE_URI[parameter_type]
        + '">\n'
    )
    encrypted_xml_string += (
        '<EncryptionMethod Algorithm="'
        + BLOCK_ENCRYPTION_ALGORITHM_URI[block_encryption_algorithm]
        + '"/>\n'
    )
    encrypted_xml_string += '<KeyInfo xmlns="' + XML_URI["dsig"] + '">\n'
    encrypted_xml_string += '<EncryptedKey xmlns="' + XML_URI["enc"] + '">\n'
    encrypted_xml_string += (
        '<EncryptionMethod Algorithm="'
        + KEY_TRANSPORT_ALGORITHM_URI[key_transport_algorithm]
        + '"/>\n'
    )
    encrypted_xml_string += '<KeyInfo xmlns="' + XML_URI["dsig"] + '">\n'
    encrypted_xml_string += "<KeyName/>\n"
    encrypted_xml_string += "</KeyInfo>\n"
    encrypted_xml_string += "<CipherData>\n"
    encrypted_xml_string += "<CipherValue>" + cipher_key + "</CipherValue>\n"
    encrypted_xml_string += "</CipherData>\n"
    encrypted_xml_string += "</EncryptedKey>\n"
    encrypted_xml_string += "</KeyInfo>\n"
    encrypted_xml_string += "<CipherData>\n"
    encrypted_xml_string += "<CipherValue>" + cipher_data + "</CipherValue>\n"
    encrypted_xml_string += "</CipherData>\n"
    encrypted_xml_string += "</EncryptedData>\n"

    return encrypted_xml_string


def triple_des_cbc(input_file):
    """Renvoie le chiffrement 3DES-CBC d'un fichier input_file, puis encodé en base64"""
    data = input_file
    data = bytes(data, "utf-8")
    # Key : 192 bits = 24 bytes
    # iv : 64 bits = 8 bytes
    key = os.urandom(24)
    iv = os.urandom(8)

    # PKCS5 PADDING: Pad with bytes all of the same value as the number of padding bytes
    # Example:
    # end of DES INPUT BLOCK  = f  o  r  _  _  _  _  _
    # (IN HEX)                  66 6F 72 05 05 05 05 05
    n = len(data)
    block_size = 8
    size_padding = block_size - (n % block_size)
    for _i in range(size_padding):
        data += bytes.fromhex("0" + str(size_padding))

    cipher = Cipher(algorithms.TripleDES(key), modes.CBC(iv), default_backend())
    encryptor = cipher.encryptor()
    encoded_data = encryptor.update(data)
    return base64.b64encode(iv + encoded_data), key


def encrypt_des_key_with_rsa_1_5(des_key, public_rsa_key):
    encrypted_des_key = public_rsa_key.encrypt(des_key, padding.PKCS1v15())
    return base64.b64encode(encrypted_des_key)


def get_public_rsa_key(private_rsa_key_file):
    """Return the public key from a private_key"""
    private_key = serialization.load_pem_private_key(
        private_rsa_key_file.read(), password=None, backend=default_backend()
    )
    public_key = private_key.public_key()
    return public_key


def get_cipherkey_ciphertext(message, public_rsa_key):
    """Return the cipher_key, cipher_text (to put in the xml encrypted file)"""
    # b64 encoded data by the symmetric algorithm (3DES-CBC), DES key
    b64_encoded_data, clear_des_key = triple_des_cbc(message)

    # b64 encoded key by rsa_1_5
    b64_encoded_des_key = encrypt_des_key_with_rsa_1_5(clear_des_key, public_rsa_key)

    return b64_encoded_des_key, b64_encoded_data


def xml_encryption_str_input(message, public_rsa_key):
    """Return the encrypted XML to string format. XML encryption with a string input"""
    # Blocks to put in the xml template
    b64_encoded_des_key, b64_encoded_data = get_cipherkey_ciphertext(
        message, public_rsa_key
    )

    # Creation of the XML encrypted file
    encrypted_xml_string = get_encrypted_xml_string(
        cipher_key=b64_encoded_des_key.decode(), cipher_data=b64_encoded_data.decode()
    )
    return encrypted_xml_string


def xml_encryption_file_input(clear_file, rsa_key_file):
    """Return the encrypted XML to string format. XML encryption with a file input"""
    with open(clear_file, encoding="utf8") as message_file:
        message = message_file.read()
    return xml_encryption_str_input(message, rsa_key_file)


def create_encrypted_xml_for_stub(altered_key, message):
    """Create xml file for a stub"""
    b64_encoded_data = base64.b64encode(message)
    b64_encoded_des_key = base64.b64encode(altered_key)
    encrypted_xml = get_encrypted_xml_string(
        cipher_key=b64_encoded_des_key.decode(), cipher_data=b64_encoded_data.decode()
    )
    return encrypted_xml.encode()
