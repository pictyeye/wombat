"""TCPStub allows to interact with a rootme-like server listening on
TCP."""

import socket
import codecs
from wombat.WTypes import WStub


class TCPStub(WStub):
    """TCPStub allows to interact with a rootme-like server listening on
    TCP. The constructor opens the connection and retrieves the public
    information (public key, challenge). The stub can then be used to
    submit encrypted messages."""

    def __init__(self, host, port, timeout=None):
        WStub.__init__(self)
        self.host = host
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))
        if timeout is not None:
            self.socket.settimeout(timeout)
        public_infos = self.socket.recv(16384).strip().split(b"\n")
        n = int(public_infos[0].split(b"=")[1], 16)  # "n = <n>"
        e = int(public_infos[1].split(b"=")[1], 16)  # "e = <e>"
        self.set_public_key(n, e)

        # "challenge = <challenge>"
        self.challenge = codecs.decode(public_infos[2].split(b"=")[1].strip(), "hex")

    def do_decrypt(self, c):
        """Submit a decryption request to the server. do_decrypt simply sends
        the encrypted message as an hexadecimal string. It returns the
        server answer."""

        self.socket.send(codecs.encode(c, "hex"))
        return self.socket.recv(16384).strip()
