from threading import Thread, Event
import time
import random
import pytest
from wombat.SimpleOracles import (
    TTT_Oracle,
    ExplicitOracleBehaviour,
    TimingOracleBehaviour,
    StubFromOracle,
)
from wombat.OracleAssessment import identify_oracle, identify_timing_oracle
from tcp.TCPStub import TCPStub
from tcp.TCPServer import launch_tcp_server


@pytest.fixture
def make_tcp_oracle():
    def _make_tcp_oracle(oracle, behaviour, host, port):
        stopper = Event()
        stub = StubFromOracle(oracle, behaviour)
        t = Thread(
            target=launch_tcp_server,
            args=(stub, (host, port)),
            kwargs={"stopper": stopper, "server_timeout": 1.0},
        )
        stopper.set()
        t.start()
        while stopper.is_set() and t.is_alive():
            time.sleep(0.1)
        return t, stopper

    return _make_tcp_oracle


def test_identify_tcp_server(make_tcp_oracle):
    host = "127.0.0.1"
    port = random.randint(2000, 3000)
    tcp_oracle, stopper = make_tcp_oracle(
        TTT_Oracle(), ExplicitOracleBehaviour(b"1", b"0"), host, port
    )
    try:
        stub = TCPStub(host, port, timeout=1.0)
        assert stub.e() == 65537
        assert stub.decrypt(stub.get_challenge()) == b"1"
        assert stub.decrypt(b"INVALID") == b"0"
        oracle_type, true_signal, false_signal = identify_oracle(stub, n=4)
        assert oracle_type == "TTT"
        assert true_signal == set([b"1"])
        assert false_signal == set([b"0"])
    finally:
        stopper.set()
        tcp_oracle.join()


def test_identify_timing_tcp_server(make_tcp_oracle):
    host = "127.0.0.1"
    port = random.randint(3000, 4000)
    tcp_oracle, stopper = make_tcp_oracle(
        TTT_Oracle(), TimingOracleBehaviour(0.1, 0.2), host, port
    )
    try:
        stub = TCPStub(host, port)
        oracle_type, low_value, high_value = identify_timing_oracle(stub)
        assert oracle_type == "TTT"
        assert low_value < 0.15
        assert high_value is None
    finally:
        stopper.set()
        tcp_oracle.join()


def test_identify_bad_tcp_server(make_tcp_oracle):
    host = "127.0.0.1"
    port = random.randint(4000, 5000)
    tcp_oracle, stopper = make_tcp_oracle(
        TTT_Oracle(), TimingOracleBehaviour(0, 0), host, port
    )
    try:
        stub = TCPStub(host, port)
        oracle_type, _true_signals, _false_signals = identify_oracle(stub)
        assert oracle_type == "Unknown"
    finally:
        stopper.set()
        tcp_oracle.join()
