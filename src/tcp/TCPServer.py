#!/usr/bin/env python3

"""TCPServer is a program proposing a simple rootme implementation,
which consists in a TCP server accepting connections. When a client
connects, the server displays the used RSA public key (n, e) and a
encrpyted message containing a secret string (the challenge).

The client can then submit encrypted values, and observe the behaviour
of the server.

All integers values are represented by hexadecimal strings.
"""

import binascii
import codecs
import sys
import socket
from threading import Thread, Event
import argparse
from wombat.WTypes import WOracle
from wombat.SimpleOracles import (
    ExplicitOracleBehaviour,
    StubFromOracle,
    TTT_Oracle,
    FTT_Oracle,
    TFT_Oracle,
    FFT_Oracle,
)


def handle_client(conn, stopper, client_number, stub, verbose):
    """This function handles a client throughout its connection. Its
    arguments are:

        - conn, the open connection,
        - stopper, an Event object allowing to stop the client
        - client_number
        - stub, the underlying connector to the implementation we
        - verbose, to display more information

    First, the function displays n, e and the challenge. Then for each
    encrypted message submitted by the peer, we give it to the oracle
    and produce the corresponding behaviour to be returned to the
    client.

    """

    nreqs = 0
    try:
        conn.send(
            b"n = %x\ne = %x\nchallenge = %s\n\n"
            % (stub.n(), stub.e(), codecs.encode(stub.get_challenge(), "hex"))
        )
        while True:
            if stopper.is_set():
                break
            msg = conn.recv(16384).strip()
            if msg == b"":
                conn.close()
                break
            nreqs += 1
            try:
                answer = stub.decrypt(codecs.decode(msg, "hex"))
                conn.send(b"%s\n" % answer)
            except binascii.Error:
                conn.send(b"Invalid input\n")
            if verbose and nreqs % 1000 == 0:
                print("[Client {client_number}] {nreqs} request(s)")
    except OSError as err:
        if verbose:
            print(
                f"[Client {client_number}] Error while handling the socket (Reason: {err})"
            )
        conn.close()
    if verbose:
        print(f"[Client {client_number}] Final number: {nreqs} request(s)")
    conn.close()


# pylint: disable=too-many-arguments
def launch_tcp_server(
    stub, address, stopper=Event(), timeout=None, server_timeout=None, verbose=None
):
    """This function launches a TCP server and lets handle_client handle
    each new connection in a separate threas. Its arguments are:

        - stub, the underlying connector to the implementation we
        - address, the bind address
        - stopper, an Event object allowing to stop the client
        - timeout, a maximum delay to wait for each client socket
        - server_timeout, a maximum delay to wait for the server socket
        - verbose, to display more information
    """

    if verbose:
        print(f"[+] Listening on {address[0]}:{address[1]}... ", end="")
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(address)
    server_socket.listen(5)
    if server_timeout is not None:
        server_socket.settimeout(server_timeout)
    if verbose:
        print("OK")

    client_number = 0
    stopper.clear()

    while not stopper.is_set():
        try:
            s, client_address = server_socket.accept()
            if timeout is not None:
                s.settimeout(timeout)
            if verbose:
                print(f"[Client {client_number}] Connection from {client_address}")
            t = Thread(
                target=handle_client, args=(s, stopper, client_number, stub, verbose)
            )
            t.start()
        except OSError as err:
            if verbose:
                print(f"[Client {client_number}] Failed (Reason: {err})")
        client_number += 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Starts a rootme-like TCP server exposing a vulnerable RSA implementation"
    )
    parser.add_argument(
        "-T",
        action="store",
        type=str,
        dest="oracle_type",
        default="TTT",
        help="set the oracle type (default is TTT)",
    )
    parser.add_argument(
        "--address",
        action="store",
        type=str,
        dest="listen_address",
        default="127.0.0.1",
        help="set the address to listen to (default is 127.0.0.1)",
    )
    parser.add_argument(
        "--port",
        action="store",
        type=int,
        dest="listen_port",
        default=12345,
        help="set the port to use (default is 12345)",
    )
    parser.add_argument(
        "--timeout",
        action="store",
        type=int,
        dest="timeout",
        default=None,
        help="maximum delay before closing an inactive socket (default is None)",
    )
    parser.add_argument(
        "--server-timeout",
        action="store",
        type=int,
        dest="server_timeout",
        default=None,
        help="maximum delay before closing the inactive server socket (default is None)",
    )
    parser.add_argument(
        "-m",
        action="store",
        type=str,
        dest="message",
        default="This is a TRUE secret. Can you find it?",
        help="define the secret message used for the challenge",
    )
    parser.add_argument(
        "-v",
        action="store_true",
        dest="verbose",
        default=False,
        help="display debug information",
    )

    args = parser.parse_args()

    if args.oracle_type == "TTT":
        oracle: WOracle = TTT_Oracle()
    elif args.oracle_type == "FTT":
        oracle = FTT_Oracle()
    elif args.oracle_type == "TFT":
        oracle = TFT_Oracle()
    elif args.oracle_type == "FFT":
        oracle = FFT_Oracle()
    else:
        print(f"Unknown oracle type: {args.oracle_type}")
        sys.exit(1)

    oracle.challenge_plaintext = args.message.encode()
    stub = StubFromOracle(oracle, ExplicitOracleBehaviour(b"1", b"0"))

    launch_tcp_server(
        stub,
        (args.listen_address, args.listen_port),
        timeout=args.timeout,
        server_timeout=args.server_timeout,
        verbose=args.verbose,
    )
