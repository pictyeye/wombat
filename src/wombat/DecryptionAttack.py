from typing import Any, Tuple, List, Union
from math import gcd
from wombat.WTypes import WOracle


def interval(a: int, b: int) -> range:
    return range(a, b + 1)


def modinv(e: int, m: int) -> int:
    """
    Modular Multiplicative Inverse. Returns x such that: (x*e) mod m == 1
    """
    x1, x2 = 1, 0
    a, b = e, m
    while b > 0:
        q, r = divmod(a, b)
        xn = x1 - q * x2
        a, b, x1, x2 = b, r, x2, xn
    return x1 % m


def ppcm(a: int, b: int) -> int:
    return a * b // gcd(a, b)


# pylint: disable=too-few-public-methods
class AttackAlgo:
    OriginalAttack = 0
    OptimisedAttack = 1
    OptimisedAttack_without_step1 = 2


# pylint: disable=too-few-public-methods
class AttackOptions:
    RawDecrypt = 1
    Debug = 0x10


class DecryptionAttack:
    def __init__(
        self, oracle: WOracle, options: int = 0, algo: int = AttackAlgo.OriginalAttack
    ):
        self.oracle = oracle
        self.options = options
        self.algo = algo
        # pylint: disable=invalid-name
        self.n, self.e = oracle.n(), oracle.e()
        B = 1 << (8 * (oracle.nlen() - 2))
        self.B2 = 2 * B
        self.B3 = self.B2 + B

    def print_debug(self, format_str: str, args: Any) -> None:
        if self.options & AttackOptions.Debug != 0:
            print(format_str % args)

    def test_s(self, c0: int, s: int) -> bool:
        return self.oracle.is_pkcs1_v15_conformant(
            (c0 * pow(s, self.e, self.n)) % self.n
        )

    def blinding(self, c0: int) -> int:
        s_new = 2
        while not self.test_s(c0, s_new):
            s_new += 1
        return s_new

    def search_u(self, t: int, c0: int) -> Tuple[int, int]:
        u_max = 3 * t // 2
        tab_u = []
        for u in range(u_max):
            if float(u) / t < 3.0 / 2 and float(u) / t > 2.0 / 3 and gcd(u, t) == 1:
                s_new = (u * modinv(t, self.n)) % self.n
                if self.test_s(c0, s_new):
                    tab_u.append(u)
        return min(tab_u), max(tab_u)

    def step1(self, c0: int) -> Tuple[int, Union[Tuple[int, int], None]]:
        t_max = 2 * self.n // (3 * self.B3)
        self.print_debug("t_max = %d", t_max)
        t = 1
        list_t: List[int] = []
        while t <= t_max and len(list_t) < 3:
            u_max = 3 * t // 2
            for u in range(u_max):
                if float(u) / t < 3.0 / 2 and float(u) / t > 2.0 / 3 and gcd(u, t) == 1:
                    s_new = (u * modinv(t, self.n)) % self.n
                    self.print_debug("(u, t) = (%d, %d)", (u, t))
                    if self.test_s(c0, s_new):
                        list_t.append(t)
                        self.print_debug("t (appended to list) = %d", t)
            t += 1
        if not list_t:
            return t, None
        lcm = list_t[0]
        for x in list_t[1:]:
            lcm = ppcm(lcm, x)
        return lcm, self.search_u(lcm, c0)

    def step2a_original(self, c0: int) -> int:
        s_new = (self.n // self.B3) + 1
        while not self.test_s(c0, s_new):
            s_new += 1
        return s_new

    def step2a_optimised(self, c0: int, set_m_old: set) -> int:
        a, b = next(iter(set_m_old))
        s_min = (self.n + self.B2) // b + 1
        j = 0
        while True:
            for s_new in interval(
                (self.B2 + j * self.n) // b, (self.B3 + j * self.n) // a
            ):
                if s_new >= s_min and self.test_s(c0, s_new):
                    return s_new
            j += 1

    def step2b(self, c0: int, s_old: int) -> int:
        s_new = s_old + 1
        while not self.test_s(c0, s_new):
            s_new += 1
        return s_new

    def step2c(self, c0: int, s_old: int, set_m_old: set) -> int:
        a, b = next(iter(set_m_old))
        r = 2 * ((s_old * b - self.B2) // self.n) + 1
        while True:
            s_min = (self.B2 + r * self.n) // b
            s_max = (self.B3 + r * self.n) // a
            for s in interval(s_min, s_max):
                if self.test_s(c0, s):
                    return s
            r += 1

    def step3(self, set_m_old: set, s_new: int) -> set:
        set_m_new = set()
        for a, b in set_m_old:
            r_min = ((a * s_new - self.B3 + 1) // self.n) + 1
            r_max = (b * s_new - self.B2) // self.n
            for r in interval(r_min, r_max):
                new_lb = max(a, ((self.B2 + r * self.n) // s_new) + 1)
                new_ub = min(b, (self.B3 - 1 + r * self.n) // s_new)
                if new_lb <= new_ub:  # the intersection must not be empty
                    set_m_new |= {(new_lb, new_ub)}
        return set_m_new

    # pylint: disable=too-many-branches
    def run(self, c: Union[bytes, int]) -> bytes:
        if not isinstance(c, int):
            c = self.oracle.int_of_bytes(c)

        s_old = 1
        if self.options & AttackOptions.RawDecrypt != 0:
            s = self.blinding(c)
            c = (c * pow(s, self.e, self.n)) % self.n
            s_old = s

        set_m_old = {(self.B2, self.B3 - 1)}
        if self.algo == AttackAlgo.OptimisedAttack:
            t, minMax_u = self.step1(c)
            if minMax_u is not None:
                set_m_old = {
                    ((self.B2 * t) // minMax_u[0], ((self.B3 - 1) * t) // minMax_u[1])
                }
            s_new = self.step2a_optimised(c, set_m_old)
        elif self.algo == AttackAlgo.OptimisedAttack_without_step1:
            s_new = self.step2a_optimised(c, set_m_old)
        elif self.algo == AttackAlgo.OriginalAttack:
            s_new = self.step2a_original(c)
        else:
            raise Exception(f"Unknown attack algorithm ({self.algo}).")
        self.print_debug("step2a ==> %d", s_new)

        while True:
            if len(set_m_old) > 1:
                s_new = self.step2b(c, s_old)
                self.print_debug("step2b ==> %d", s_new)
            else:
                s_new = self.step2c(c, s_old, set_m_old)
                self.print_debug("step2c ==> %d", s_new)

            set_m_new = self.step3(set_m_old, s_new)
            self.print_debug("step3 ==> %s", str(set_m_new))

            if len(set_m_new) == 1:
                a, b = next(iter(set_m_new))
                if a == b:
                    if self.options & AttackOptions.RawDecrypt != 0:
                        m = (a * modinv(s, self.n)) % self.n
                        padded_msg = self.oracle.bytes_of_int(m)
                        return padded_msg
                    padded_msg = self.oracle.bytes_of_int(a)
                    index = padded_msg.find(0, 10)
                    if index < 0:
                        raise Exception("Invalid padding")
                    return padded_msg[index + 1 :]

            s_old = s_new
            set_m_old = set_m_new
