"""The WTypes module describe essential classes for wombat.

It contains the base class, WPubKeyHolder, as well as the WStub and
the WOracle classes and useful exceptions."""

import codecs
import time
import typing
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.backends import default_backend


class NoPublicKey(Exception):
    """Some operations require a public key to be set."""


class NoPrivateKey(Exception):
    """Some operations require the private key to be set."""


class NoChallenge(Exception):
    """get_challenge was called but no challenge was defined."""


class VirtualFunctionCalled(Exception):
    """Some operations are not defined in base classes and should be
    refined in derived classes."""


class InputIsTooBig(Exception):
    """Some functions check that their parameters are within acceptable
    bounds. If such tests fail, this exception is raised."""


class TooManyCalls(Exception):
    """The calls made to the oracle have reached the limit."""


class WPubKeyHolder:
    """WPubKeyHolder is a base class for Wombat objects holding an RSA
    public key."""

    def __init__(self):
        self.private_key_object = None
        self.challenge_plaintext = None
        self.challenge = None
        self.public_key_object = None

    def set_public_key(self, n: int, e: int):
        """Set the public key using its parameters n and e"""

        pubkey_numbers = rsa.RSAPublicNumbers(e, n)
        self.public_key_object = default_backend().load_rsa_public_numbers(
            pubkey_numbers
        )

    def get_public_key(self):
        """Return the RSA public key stored in the object, or raises
        NoPublicKey in case no key was defined."""

        if self.public_key_object is None:
            if self.private_key_object is None:
                raise NoPublicKey
            self.public_key_object = self.private_key_object.public_key()
        return self.public_key_object

    def n(self) -> int:
        """Return the RSA modulus, or raises NoPublicKey in case no key was
        defined."""

        return self.get_public_key().public_numbers().n

    def e(self) -> int:
        """Return the RSA public exponent, or raises NoPublicKey in case no
        key was defined."""

        return self.get_public_key().public_numbers().e

    def nlen(self) -> int:
        """Return the byte length of the RSA modulus, or raises NoPublicKey in
        case no key was defined."""

        return (self.get_public_key().key_size + 7) // 8

    def bytes_of_int(self, x_int: int) -> bytes:
        """Helper function to convert a big integer into a bytes object, using
        the expected byte length. This function can raise the
        NoPublicKey exception in case no key was defined."""

        if x_int >= self.n():
            raise InputIsTooBig
        result = b"%x" % x_int
        result = b"0" * (self.nlen() * 2 - len(result)) + result
        return codecs.decode(result, "hex")

    def int_of_bytes(self, x_str: bytes) -> int:
        """Helper function to convert a bytes object into big integer, using
        the expected byte length. This function can raise the
        NoPublicKey exception in case no key was defined."""

        if len(x_str) > self.nlen():
            raise InputIsTooBig
        x_hex = codecs.encode(x_str, "hex")
        x_int = int(x_hex, 16)
        if x_int >= self.n():
            raise InputIsTooBig
        return x_int

    def encrypt(self, m: bytes) -> bytes:
        """Encrypt the message m with the public key defined in the object,
        using the standard PKCS#1 v1.5 padding type 2. In case no key
        was defined, the NoPublicKey exception is raised."""

        k = self.get_public_key()
        return k.encrypt(m, padding.PKCS1v15())

    def raw_decrypt(self, c: bytes) -> bytes:
        """Decrypt the message c with the private key defined in the object,
        if it is present.  If it is absent, the NoPrivateKey exception is
        raised. This is useful for some tests."""

        if self.private_key_object is None:
            raise NoPrivateKey
        d = self.private_key_object.private_numbers().d
        plaintext_int = pow(self.int_of_bytes(c), d, self.n())
        return self.bytes_of_int(plaintext_int)

    def get_challenge(self) -> bytes:
        """Return an encrypted challenge. challenge or challenge_plaintext
        should be overriden by specialized classes to represent
        realistic encrypted messages. In case no challenge was defined, the
        NoChallenge exception is raised."""

        if self.challenge is None:
            if self.challenge_plaintext is None:
                raise NoChallenge
            self.challenge = self.encrypt(self.challenge_plaintext)
        return self.challenge


class WStub(WPubKeyHolder):
    """WStub describes a stub (or a connector) to submit encrypted
    messages to a given implementation. It is the base tool to
    interact with a PKCS#1 v1.5 implementation to test it.

    To work with a given implementation, you must define a WStub
    subclass, and refine the relevant methods. You must at least write
    a do_decrypt method to handle the decryption per se. You may need
    to override the constructor, if needed. Moreover, if you are
    interested in timing oracles, you might want to split the
    decryption operation into three steps (prepare_decrypt, do_decrypt
    and cleanup_decrypt), to restrict do_decrypt (whose execution time
    will be measured) to the real decryption operation."""

    def __init__(self):
        WPubKeyHolder.__init__(self)
        self.n_decrpytion_requests = 0

    def prepare_decrypt(self):
        """This method is supposed to isolate the steps related to decryption
        that can be isolated before triggering the actual decryption
        operation. Thus, when measuring the execution time of
        do_decrypt, we can obtain more precise figures."""

    def do_decrypt(self, _c: bytes):
        """This method triggers the actual decryption operation. If possible,
        it should be reduced as much as possible, to let the steps
        required before and after the decryption in prepare_decrypt
        and cleanup_decrypt respectively. This function should return
        the elements describing the behaviour of the implementation
        under test."""

        raise VirtualFunctionCalled("Virtual WStub can not decrypt.")

    def cleanup_decrypt(self):
        """This method is supposed to isolate the steps related to decryption
        that can be isolated after triggering the actual decryption
        operation. Thus, when measuring the execution time of
        do_decrypt, we can obtain more precise figures. Sometimes, to
        avoid having long execution times in do_decrypt, you can save
        the raw result in do_decrypt and use cleanup_decrypt to do
        postprocessing on the recorded behaviour of the tested
        implementation."""

    def decrypt(self, c: bytes) -> bytes:
        """This method is called by identify_oracle and other Wombat tools to
        submit a decryption request to the test
        implementation. However, it is important to remember that it
        should NOT be overriden. You should instead override
        do_decrypt (and optionally prepare_decrypt and
        cleanup_decrypt). decrypt returns the result of do_decrypt or,
        in case it is None, the result of cleanup_decrypt."""

        self.prepare_decrypt()
        # pylint: disable=assignment-from-no-return
        result_do_decrypt = self.do_decrypt(c)
        self.n_decrpytion_requests += 1
        # pylint: disable=assignment-from-no-return
        result_cleanup_decrypt = self.cleanup_decrypt()
        if result_do_decrypt is None:
            return result_cleanup_decrypt
        return result_do_decrypt

    def decryption_time(self, c: bytes) -> float:
        """This method is called by identify_timing_oracle and other Wombat
        tools to submit a decryption request and measure the time
        spent in do_decrypt. As the decrypt method, it should NOT be
        overriden."""

        self.prepare_decrypt()
        start = time.time()
        self.do_decrypt(c)
        end = time.time()
        self.cleanup_decrypt()
        return end - start


class WOracle(WPubKeyHolder):
    """WOracle is a WPubKeyHolder able to submit decryption requests to a
    vulnerable implementation. Contrarily to WStub, WOracle is
    supposed to always return true or false, via a specific method
    called is_pkcs1_v15_conformant."""

    def __init__(self):
        WPubKeyHolder.__init__(self)
        self.n_calls = 0
        self.max_calls = None

    def set_max_calls(self, new_max: int):
        """To avoid running long attacks that may never succeed, you can limit
        the maximum calls to the oracle you are willing to do."""

        self.max_calls = new_max

    def oracle(self, _c: bytes) -> bool:
        """This is the heart of the WOracle class, that you have to override
        to produce a useful oracle. This method takes an encrypted
        message, submits it to the vulnerable implementation and
        interpret the observed behaviour to return True or False."""

        raise VirtualFunctionCalled("Virtual WOracle does not implement any oracle.")

    def is_pkcs1_v15_conformant(self, c: typing.Union[bytes, int]) -> bool:
        """This function is the method you should call to test whether c
        corresponds to a correctly padded plaintext. You should not
        call oracle directly, since is_pkcs1_v15_conformant does some
        bookkeeping. For the same reason, you should NOT override
        is_pkcs1_v15_conformant directly, but always override oracle."""

        if self.max_calls is not None and self.n_calls >= self.max_calls:
            raise TooManyCalls
        if isinstance(c, int):
            c = self.bytes_of_int(c)
        self.n_calls += 1
        return self.oracle(c)
