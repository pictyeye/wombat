"""Tests regarding the identification and evaluation of oracles."""

import pytest
from wombat.SimpleOracles import (
    TTT_Oracle,
    FTT_Oracle,
    TFT_Oracle,
    FFT_Oracle,
    FFF_Oracle,
    ExplicitOracleBehaviour,
    TimingOracleBehaviour,
    UnknownBehaviour,
    OracleFromStub,
    StubFromOracle,
)
from wombat.OracleAssessment import (
    identify_oracle,
    identify_timing_oracle,
    evaluate_oracle,
)


int_oracle_behaviour = ExplicitOracleBehaviour(45, 44)
bool_oracle_behaviour = ExplicitOracleBehaviour(True, False)
str_oracle_behaviour = ExplicitOracleBehaviour(b"1", b"0")


# pylint: disable=invalid-name
def test_identify_TTT_oracle():
    """Check that a pure TTT oracle is correctly identified."""

    stub = StubFromOracle(TTT_Oracle(), bool_oracle_behaviour)
    oracle_type, true_signals, false_signals = identify_oracle(stub)
    assert oracle_type == "TTT"
    assert true_signals == set([True])
    assert false_signals == set([False])


# pylint: disable=invalid-name
def test_identify_TFT_oracle():
    """Check that a pure TFT oracle is correctly identified."""

    stub = StubFromOracle(TFT_Oracle(), int_oracle_behaviour)
    oracle_type, true_signals, false_signals = identify_oracle(stub, n=10)
    assert oracle_type == "TFT"
    assert true_signals == set([45])
    assert false_signals == set([44])


# pylint: disable=invalid-name
def test_identify_FTT_oracle():
    """Check that a pure FTT oracle is correctly identified."""

    stub = StubFromOracle(FTT_Oracle(), bool_oracle_behaviour)
    oracle_type, true_signals, false_signals = identify_oracle(stub)
    assert oracle_type == "FTT"
    assert true_signals == set([True])
    assert false_signals == set([False])


# pylint: disable=invalid-name
def test_identify_FFT_oracle():
    """Check that a pure FFT oracle is correctly identified."""

    stub = StubFromOracle(FFT_Oracle(), str_oracle_behaviour)
    oracle_type, true_signals, false_signals = identify_oracle(stub, n=3)
    assert oracle_type == "FFT"
    assert true_signals == set([b"1"])
    assert false_signals == set([b"0"])


# pylint: disable=invalid-name
def test_identify_FFF_oracle():
    """Check that a pure FFT oracle is correctly identified."""

    stub = StubFromOracle(FFF_Oracle(48), bool_oracle_behaviour)
    oracle_type, true_signals, false_signals = identify_oracle(stub)
    assert oracle_type == "FFF(48)"
    assert true_signals == set([True])
    assert false_signals == set([False])


def test_identification_failure():
    """Check that a non-vulnerable implementation can not be identified as
    an oracle."""

    stub = StubFromOracle(TTT_Oracle(), TimingOracleBehaviour(0.01, 0.01))
    oracle_type = identify_oracle(stub)[0]
    assert oracle_type == "Unknown"
    oracle_type = identify_timing_oracle(stub)[0]
    assert oracle_type == "Unknown"


oracles = [
    (TTT_Oracle, "TTT", [1, 0, 0, 1, 1]),
    (TFT_Oracle, "TFT", [1, 0, 0, 0, 1]),
    (FTT_Oracle, "FTT", [1, 0, 0, 1, 0]),
    (FFT_Oracle, "FFT", [1, 0, 0, 0, 0]),
]
oracle_ids = list(map(lambda o: o[1], oracles))


@pytest.mark.parametrize("oracles", oracles, ids=oracle_ids)
def test_identify_timing_oracle(oracles):
    """Check that identify_timing_oracle works as expected with pure
    oracles and a resaonnable timing difference."""

    oracle = oracles[0]()
    stub = StubFromOracle(oracle, TimingOracleBehaviour(0.2, 0.1))
    oracle_type, low_value, high_value = identify_timing_oracle(stub)
    assert low_value is None
    assert high_value > 0.14
    assert oracle_type == oracles[1]


@pytest.mark.parametrize("oracles", oracles, ids=oracle_ids)
def test_evaluate_oracle(oracles):
    """Check that evaluate_oracle returns the expected results."""

    oracle = oracles[0]()
    answers = evaluate_oracle(oracle, n=10)
    for expected, computed in zip(oracles[2], answers):
        assert 10 * expected == computed


def test_built_oracle():
    """Check that we can build an oracle from a stub, as soon as an oracle
    has been identified."""

    stub = StubFromOracle(TTT_Oracle(), bool_oracle_behaviour)
    _, true_signals, false_signals = identify_oracle(stub)
    built_oracle = OracleFromStub(stub, true_signals, false_signals)
    assert built_oracle.n() == stub.n()
    assert built_oracle.is_pkcs1_v15_conformant(built_oracle.get_challenge())
    assert not built_oracle.is_pkcs1_v15_conformant(0)

    built_oracle2 = OracleFromStub(stub, [], false_signals)
    with pytest.raises(UnknownBehaviour):
        built_oracle2.is_pkcs1_v15_conformant(built_oracle2.get_challenge())
    built_oracle2.accept_unknown_behaviour()
    assert not built_oracle2.is_pkcs1_v15_conformant(built_oracle2.get_challenge())
