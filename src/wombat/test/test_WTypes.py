"""Tests checking that WTypes classes work as expected."""

import random
import codecs
import pytest
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend

from wombat.WTypes import (
    NoPublicKey,
    NoPrivateKey,
    NoChallenge,
    InputIsTooBig,
    VirtualFunctionCalled,
    WStub,
    WOracle,
    WPubKeyHolder,
)


@pytest.fixture
def rsa_modulus():
    """Sample RSA modulus used in many tests."""
    # pylint: disable=line-too-long
    return 128892800399193854475552356365979606980398890309952048662733302476541582659925303310855048858465024039907343170267009829844663930100610365402546498655928028317521036105618722152361639605665415697292955884981384156564925148114802795400397434347384212795052964588874944289553122549316124170252391866911614550187


@pytest.fixture
def rsa_pubexp():
    """Standard RSA public exponent (F4 = 2^(2^4) + 1)"""
    return 65537


@pytest.fixture
def pub_key_holder(rsa_modulus, rsa_pubexp):
    """Sample WPubKeyHolder used in many tests."""
    res = WPubKeyHolder()
    res.set_public_key(rsa_modulus, rsa_pubexp)
    return res


@pytest.fixture
def stub(rsa_modulus, rsa_pubexp):
    """Sample Stub used in tests."""
    res = WStub()
    res.set_public_key(rsa_modulus, rsa_pubexp)
    return res


@pytest.fixture
def oracle(rsa_modulus, rsa_pubexp):
    """Sample Oracle used in tests."""
    res = WOracle()
    res.set_public_key(rsa_modulus, rsa_pubexp)
    return res


def test_key_holder_init1(pub_key_holder):
    """Check the WPubKeyHolder.nlen method."""
    assert pub_key_holder.nlen() == 128


def test_key_holder_init2(pub_key_holder):
    """Check the WPubKeyHolder.e method."""
    assert pub_key_holder.e() == 65537


def test_key_holder_init3():
    """Check that a missing public key is noticed."""
    pub_key_holder = WPubKeyHolder()
    with pytest.raises(NoPublicKey):
        pub_key_holder.get_public_key()


def test_key_holder_raw_decrypt(pub_key_holder):
    """Check that decryption only works when the private exponent is
    given."""
    with pytest.raises(NoPrivateKey):
        pub_key_holder.raw_decrypt(b"")


def test_bytes_int_bytes(pub_key_holder):
    """Check the bytes_of_int and int_of_bytes from WPubKeyHolder."""
    msg_len = random.randrange(1, 128)
    msg = ""
    for _i in range(msg_len):
        rnd = random.randrange(256)
        msg += f"{rnd:02x}"
    msg = codecs.decode(msg, "hex")
    msg = b"\x00" * (pub_key_holder.nlen() - len(msg)) + msg
    assert msg == (pub_key_holder.bytes_of_int(pub_key_holder.int_of_bytes(msg)))


def test_int_bytes_int(pub_key_holder):
    """Check the bytes_of_int and int_of_bytes from WPubKeyHolder."""
    msg = random.randrange(1, 10000000000000000)
    assert msg == (pub_key_holder.int_of_bytes(pub_key_holder.bytes_of_int(msg)))


def test_virtual_functions(stub, oracle):
    """Check that unspecified virtual functions raise excepetion."""
    with pytest.raises(VirtualFunctionCalled):
        stub.decrypt("")
    with pytest.raises(VirtualFunctionCalled):
        oracle.is_pkcs1_v15_conformant("")


def test_wrong_input_len_for_conversion_functions(pub_key_holder):
    """Check that bytes_of_int and int_of_bytes verify input with invalid
    lengths."""
    msg = b"X" * (pub_key_holder.nlen() + 1)
    with pytest.raises(InputIsTooBig):
        pub_key_holder.int_of_bytes(msg)
    msg = b"\xff" * (pub_key_holder.nlen())
    with pytest.raises(InputIsTooBig):
        pub_key_holder.int_of_bytes(msg)
    int_msg = pub_key_holder.n()
    with pytest.raises(InputIsTooBig):
        pub_key_holder.bytes_of_int(int_msg)


def test_missing_challenge(pub_key_holder):
    """Check that WPubKeyHolder raises a NoChallenge exception when
    expected."""
    with pytest.raises(NoChallenge):
        pub_key_holder.get_challenge()


def test_encrypt_decrypt():
    """Check that encryption/decryption works as expected in
    WPubKeyHolder. Of course, this only makes sense when a private key is
    used."""
    pub_key_holder = WPubKeyHolder()
    pub_key_holder.private_key_object = rsa.generate_private_key(
        65537, 1024, default_backend()
    )
    msg = b"TEST"
    encrypted_msg = pub_key_holder.encrypt(msg)
    assert len(encrypted_msg) == 1024 // 8

    decrypted_msg = pub_key_holder.raw_decrypt(encrypted_msg)
    assert len(decrypted_msg) == 1024 // 8
    assert decrypted_msg[:2] == b"\x00\x02"
    assert decrypted_msg[-5:] == b"\x00TEST"
    assert b"\x00" not in decrypted_msg[2:-5]
