import pytest
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend
from wombat.SimpleOracles import PureOracle
from wombat.DecryptionAttack import AttackAlgo, DecryptionAttack, AttackOptions

# pylint: disable=line-too-long
n_orig = 0xCC78175F33832CA8AE28AA193F9E53A3F89779FC44E4104639DF857EDF7A3F8B8CB4B1B977DA6295453C39E48BF5DE6E93619E3739378C720D04BAC5AB3B8C8F9650625B8163CA3FEDB151EBFF2B952612105B662CE63A8CD241F6FFFA5CD9B55010CFB08CD0E91FD87CFE0C2791241B5EC7CC22779319B6A9E8601073FC2917
# pylint: disable=line-too-long
d_orig = 0xA507C8D56E1E51CD53C43E866272C7CF70B5D21F6AFB28A785CE835103CF07BAFAB65D3BF520AEB10C50AFE9678DEB1A149A95B8BCD5B8E5D4DC1C63AEF14160B07A09A87367D44F8BB4A05ED4E817B0138D206266F5149EC4C3A4855DAF6AFA5E5293B075C92D9D79DEDB469A48F41031D9B5ECB262EF971BD38FDD252ED101
# pylint: disable=line-too-long
c_orig = 0x2E47A6676BCAF362795F2B09BF127D9806F804C8725C6D8EAC772A2C74AB3DCB32E1A2CBD9D0DC497D3BF4DDEE5903DDE41D3D95B2938063D4C9276ED4C511DD071C22D38624D0CC65B046205032D72842448AA391ECFE46A5F17828E1B402A064EA3F47AB278395A9BBBAB0E1350310EAF7DB02A74175F4F190DA04E00E70C2

# pylint: disable=line-too-long
n_optim = 0xC077115FEF87E61D89040733B239A332F7F2F356BE076DF7A5089B54A1E2CA887E374AFA6E41C030620D545EF0E6EC8297A41F0DAAA928075E4F4F08B08CB82B930A5188DAA0DEB551B125652F0E5225E01506CEDEB2ED32155071CD216875E161659ED3D7C5FC7195DBC6D353823569902F8493A5416765175D5FD76668AD7B
# pylint: disable=line-too-long
d_optim = 0x5F1A76FB70E1365461E01BAEEFE28B42244368FAF3EBB7EB2721999FB85ED8B599A8B8E4069776E07193249DC56C5672317C67DBD8F8BD4EE6CB49C2817B0785A5443B99286069081E87A11AFA1E8503AF355FD91558DEEED18EA13FCB8BADB0EA58093347F5A82F81BC02D1416E718EADA9FAE36429CE52613DDD150BCACC1
# pylint: disable=line-too-long
c_optim = 0x93650B3FA991F10569508F7DE671EBCEE11D4CDD08AB00C999C5F17CEA0FE775E67C37C71FB9EABEC94BBF6BA31C17A35F8D18A990D1BCC12A07D19963513C05D11802D880D117922CE5EE1FFB6D6C90B7F3083B92F9F034B9B62D95D7D9CA971E103ECC61E2A57312F46F311C46E9B47193349E3ECE02CDC6465471A06CDE56

# pylint: disable=line-too-long
n_raw = 0xC38672AF62BF0329ABE9115152E234980D70ABA0ACC839623755A35D4639C093B45D49ED784C01E4653FD5C863FD2933AE83CD6AB6E6892BBAAB9D51D2570DCF3A08A81510836B903FEC33605D39EC079822DB218553DE40AFA2522F069CAE00AD667524BECB9DF9F21DD14CDEA5EE36581B263030965AF061E86E03A32E6DEF
# pylint: disable=line-too-long
d_raw = 0x9E60E3B90985AC2C2F226572B86E9F27A38B669A8A1ACDD0AEFDE18C867BF5702FA37964B012F15D6E5E5CEF2B7F3FC6AB4FFC2EBAE4D6ABE3536BE5813A1EE6188C35972898E1BF84E24BF8665B01000AB7F245D113E84507C643BFA92A00732C6DB9E76835663E1D892FD26F1AA25431CB27944DC60378DE2D37B9BC505AC1

# pylint: disable=invalid-name
class Fixed_TTT_Oracle(PureOracle):
    def __init__(self, n, d):
        PureOracle.__init__(self, keysize=None)
        e = 65537
        p, q = rsa.rsa_recover_prime_factors(n, e, d)
        dmp1 = rsa.rsa_crt_dmp1(d, p)
        dmq1 = rsa.rsa_crt_dmq1(d, q)
        imqp = rsa.rsa_crt_iqmp(p, q)
        public_numbers = rsa.RSAPublicNumbers(e, n)
        privnums = rsa.RSAPrivateNumbers(p, q, d, dmp1, dmq1, imqp, public_numbers)
        self.private_key_object = default_backend().load_rsa_private_numbers(privnums)

    def call(self, plaintext):
        return plaintext[:2] == b"\x00\x02"


algos_and_params = [
    (AttackAlgo.OptimisedAttack, n_optim, d_optim, c_optim),
    pytest.param(
        (AttackAlgo.OriginalAttack, n_orig, d_orig, c_orig), marks=pytest.mark.slow
    ),
    pytest.param(
        (AttackAlgo.OptimisedAttack_without_step1, n_orig, d_orig, c_orig),
        marks=pytest.mark.slow,
    ),
]
algo_ids = ["Optimised", "Original", "Optimized w/o step1"]


@pytest.mark.parametrize("algos_and_params", algos_and_params, ids=algo_ids)
def test_decryption_attack(algos_and_params):
    algo, n, d, c = algos_and_params
    oracle = Fixed_TTT_Oracle(n, d)
    oracle.set_max_calls(6000)
    attack = DecryptionAttack(oracle, algo=algo)
    msg = attack.run(c)
    assert msg == b"Can you keep a secret?"


@pytest.mark.slow
def test_raw_decryption_attack():
    oracle = Fixed_TTT_Oracle(n_raw, d_raw)
    oracle.set_max_calls(10000)
    attack = DecryptionAttack(
        oracle, algo=AttackAlgo.OptimisedAttack, options=AttackOptions.RawDecrypt
    )
    msg = b"Hello World!"
    almost_signed_msg = oracle.int_of_bytes(attack.run(msg))
    assert oracle.int_of_bytes(msg) == pow(almost_signed_msg, oracle.e(), oracle.n())
