"""Tests for the helper functions in ConstructMsg."""

import pytest

from wombat.WTypes import InputIsTooBig
from wombat.ConstructMsg import (
    change_second_byte,
    shorten_padding,
    extend_padding_to_the_end,
    change_msg_len,
    produce_altered_message,
)

# pylint: disable=unused-import, wrong-import-order
from .test_WTypes import rsa_modulus, rsa_pubexp, pub_key_holder


def test_len_failsafe(pub_key_holder):
    """Check that a message too long is caught by produce_altered_message."""
    with pytest.raises(InputIsTooBig):
        produce_altered_message(lambda x: x, pub_key_holder, cleartext=(b"1" * 10000))


def test_change_second_byte():
    """Check that change_second_byte works as expected."""

    m = b"\x00\x02tititoto"
    altered1 = change_second_byte(m)
    assert altered1[0] == m[0] and altered1[1] != m[1] and altered1[2:] == m[2:]


def test_shorten_padding():
    """Check that shorten_padding works as expected."""

    m = b"\x00\x02tititoto"
    altered2 = shorten_padding(m)
    assert altered2[0:2] == m[0:2] and altered2[10:] == m[10:]
    n_zeroes = 0
    for i in range(2, 10):
        if altered2[i] == 0:
            n_zeroes += 1
            assert m[i] != 0
        else:
            assert altered2[i] == m[i]
    assert n_zeroes == 1


def test_change_msg_len():
    """Check that change_msg_len works as expected."""

    m = b"\x00\x02_ABCDEFGH\x00tititoto"
    expected1 = b"\x00\x02_ABCDEFGH\xff\x00ititoto"
    assert change_msg_len(m, 7) == expected1
    expected2 = b"\x00\x02_ABCDEFG\x00\x00tititoto"
    assert change_msg_len(m, 9) == expected2
    assert change_msg_len(m, 8) == m

    m2 = b"\x00\x02_0123456789_ABCDEF"
    expected3 = b"\x00\x02_0123456789\x00ABCDEF"
    assert change_msg_len(m2, 6) == expected3


def test_remove_00_from_message():
    """Check that extend_padding_to_the_end works as expected."""

    b = b"\x00\x02abcd\x00efgh\x00"
    expected = b"\x00\x02abcd\x01efgh\x01"
    assert extend_padding_to_the_end(b) == expected
