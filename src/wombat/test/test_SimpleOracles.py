"""Tests against classes from SimpleOracles"""

import pytest

from wombat.WTypes import VirtualFunctionCalled, TooManyCalls

from wombat.ConstructMsg import (
    alter_challenge,
    change_second_byte,
    shorten_padding,
    extend_padding_to_the_end,
    change_msg_len,
)

from wombat.SimpleOracles import (
    PureOracle,
    TTT_Oracle,
    TFT_Oracle,
    FTT_Oracle,
    FFT_Oracle,
    FFF_Oracle,
)


def test_alter_challenge_id():
    """Check that alter_challenge does nothing with the identity as its f
    parameter."""
    oracle = TTT_Oracle()
    c = oracle.get_challenge()
    c2 = alter_challenge(lambda x: x, oracle)
    assert c == c2


# pylint: disable=invalid-name
def test_TTT_Oracle():
    """Check that pure TTT oracles behave as expected."""

    oracle = TTT_Oracle()

    c = oracle.get_challenge()
    assert oracle.is_pkcs1_v15_conformant(c)

    c1 = alter_challenge(change_second_byte, oracle)
    assert not oracle.is_pkcs1_v15_conformant(c1)

    c2 = alter_challenge(shorten_padding, oracle)
    assert oracle.is_pkcs1_v15_conformant(c2)

    c3 = alter_challenge(extend_padding_to_the_end, oracle)
    assert oracle.is_pkcs1_v15_conformant(c3)

    c_int = oracle.int_of_bytes(c)
    assert oracle.is_pkcs1_v15_conformant(c_int)


# pylint: disable=invalid-name
def test_TFT_Oracle():
    """Check that pure TFT oracles behave as expected."""

    oracle = TFT_Oracle()

    c = oracle.get_challenge()
    assert oracle.is_pkcs1_v15_conformant(c)

    c1 = alter_challenge(change_second_byte, oracle)
    assert not oracle.is_pkcs1_v15_conformant(c1)

    c2 = alter_challenge(shorten_padding, oracle)
    assert not oracle.is_pkcs1_v15_conformant(c2)

    c3 = alter_challenge(extend_padding_to_the_end, oracle)
    assert oracle.is_pkcs1_v15_conformant(c3)


# pylint: disable=invalid-name
def test_FTT_Oracle():
    """Check that pure FTT oracles behave as expected."""

    oracle = FTT_Oracle()

    c = oracle.get_challenge()
    assert oracle.is_pkcs1_v15_conformant(c)

    c1 = alter_challenge(change_second_byte, oracle)
    assert not oracle.is_pkcs1_v15_conformant(c1)

    c2 = alter_challenge(shorten_padding, oracle)
    assert oracle.is_pkcs1_v15_conformant(c2)

    c3 = alter_challenge(extend_padding_to_the_end, oracle)
    assert not oracle.is_pkcs1_v15_conformant(c3)


# pylint: disable=invalid-name
def test_FFT_Oracle():
    """Check that pure FFT oracles behave as expected."""

    oracle = FFT_Oracle()

    c = oracle.get_challenge()
    assert oracle.is_pkcs1_v15_conformant(c)

    c1 = alter_challenge(change_second_byte, oracle)
    assert not oracle.is_pkcs1_v15_conformant(c1)

    c2 = alter_challenge(shorten_padding, oracle)
    assert not oracle.is_pkcs1_v15_conformant(c2)

    c3 = alter_challenge(extend_padding_to_the_end, oracle)
    assert not oracle.is_pkcs1_v15_conformant(c3)


# pylint: disable=invalid-name
def test_FFF_Oracle():
    """Check that pure FFT oracles behave as expected."""

    oracle = FFF_Oracle(48)

    c = oracle.get_challenge()
    assert oracle.is_pkcs1_v15_conformant(c)

    c1 = alter_challenge(change_second_byte, oracle)
    assert not oracle.is_pkcs1_v15_conformant(c1)

    c2 = alter_challenge(shorten_padding, oracle)
    assert not oracle.is_pkcs1_v15_conformant(c2)

    c3 = alter_challenge(extend_padding_to_the_end, oracle)
    assert not oracle.is_pkcs1_v15_conformant(c3)

    c4 = alter_challenge(lambda msg: change_msg_len(msg, 32), oracle)
    assert not oracle.is_pkcs1_v15_conformant(c4)

    c5 = alter_challenge(lambda msg: change_msg_len(msg, 64), oracle)
    assert not oracle.is_pkcs1_v15_conformant(c5)


def test_max_calls():
    """Check that using set_max_calls triggers an exception as expected."""

    oracle = TTT_Oracle()
    oracle.set_max_calls(1)
    c = oracle.get_challenge()
    assert oracle.n_calls == 0

    assert oracle.is_pkcs1_v15_conformant(c)
    assert oracle.n_calls == 1

    with pytest.raises(TooManyCalls):
        oracle.is_pkcs1_v15_conformant(c)

    assert oracle.n_calls == 1


def test_virtual_functions():
    """Check that calling an unimplented virtual function raises an
    exception."""

    o = PureOracle()
    with pytest.raises(VirtualFunctionCalled):
        o.is_pkcs1_v15_conformant(o.get_challenge())
