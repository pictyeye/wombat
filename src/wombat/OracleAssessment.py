from typing import Any, Callable, List, Tuple, Set, Optional
from wombat.WTypes import WStub, WOracle
from wombat.ConstructMsg import (
    produce_altered_message,
    change_second_byte,
    shorten_padding,
    extend_padding_to_the_end,
    random_msg,
    change_byte,
)


def decrypt(stub: WStub, msg: bytes):
    try:
        return stub.decrypt(msg)
    # pylint: disable=broad-except
    except Exception as e:
        return f"Exception: {e}"


def decryption_time(stub: WStub, msg: bytes):
    try:
        return stub.decryption_time(msg)
    # pylint: disable=broad-except
    except Exception:
        return None


msg_ok = 0
msg_02_modif = 1
msg_short_pad = 2
msg_null_msg = 3


def iter_decryption(
    n: int, stub: WStub, decrypt_function: Callable[[WStub, bytes], Any]
) -> List[Any]:
    answers: List[Any] = [set(), set(), set(), set()]
    stimulus: List[bytes] = [b""] * 4

    for _i in range(n):
        cleartext_challenge = random_msg(stub, None)
        stimulus[msg_ok] = produce_altered_message(
            lambda x: x, stub, cleartext_challenge
        )
        stimulus[msg_02_modif] = produce_altered_message(
            change_second_byte, stub, cleartext_challenge
        )
        stimulus[msg_short_pad] = produce_altered_message(
            shorten_padding, stub, cleartext_challenge
        )
        stimulus[msg_null_msg] = produce_altered_message(
            extend_padding_to_the_end, stub, cleartext_challenge
        )

        for j in range(4):
            a = decrypt_function(stub, stimulus[j])
            if a is not None:
                answers[j].add(a)

    return answers


def iter_decryption_with_varlen_msgs(
    n: int, stub: WStub, decrypt_function: Callable[[WStub, bytes], Any]
) -> List[Any]:
    maxlen = stub.nlen() - 11
    answers: List[Any] = []
    for l in range(maxlen + 1):
        answers.append(set())

    for _i in range(n):
        for l in range(maxlen + 1):
            cleartext_challenge = random_msg(stub, l)
            stimulus = produce_altered_message(lambda x: x, stub, cleartext_challenge)
            a = decrypt_function(stub, stimulus)
            if a is not None:
                answers[l].add(a)

    return answers


def identify_oracle(stub: WStub, n: int = 1) -> Tuple[str, List[Any], List[Any]]:
    rsps = iter_decryption(n, stub, decrypt)
    oracle_type = ""
    good_msgs = rsps[msg_ok].difference(rsps[msg_02_modif])
    if len(good_msgs) > 0:
        new_msgs = rsps[msg_null_msg].difference(rsps[msg_02_modif])
        if len(new_msgs) > 0:
            oracle_type += "T"
            good_msgs = good_msgs.union(new_msgs)
        else:
            oracle_type += "F"

        new_msgs = rsps[msg_short_pad].difference(rsps[msg_02_modif])
        if len(new_msgs) > 0:
            oracle_type += "T"
            good_msgs = good_msgs.union(new_msgs)
        else:
            oracle_type += "F"

        oracle_type += "T"
        bad_msgs = (
            rsps[msg_ok]
            .union(rsps[msg_02_modif], rsps[msg_short_pad], rsps[msg_null_msg])
            .difference(good_msgs)
        )
        return oracle_type, good_msgs, bad_msgs

    # We are facing an FFF oracle, or the implementation is not an oracle
    rsps = iter_decryption_with_varlen_msgs(n, stub, decrypt)
    for l, curlen_answers in enumerate(rsps):
        otherlen_answers: Set[Any] = set()
        for i, msgs in enumerate(rsps):
            if i != l:
                otherlen_answers = otherlen_answers.union(msgs)
        good_msgs = curlen_answers.difference(otherlen_answers)
        # Warning: currently, we assume that FFF is only true for one specific length
        if good_msgs:
            bad_msgs = curlen_answers.union(otherlen_answers).difference(good_msgs)
            return f"FFF({l})", good_msgs, bad_msgs
    return "Unknown", [], []


def observable_signals(good: List[float], min_bad: float, max_bad: float):
    return (
        set(filter(lambda x: x < min_bad, good)),
        set(filter(lambda x: x > max_bad, good)),
    )


def identify_timing_oracle(stub: WStub, n: int = 3, minimum_delta: float = 0.05):
    rsps = iter_decryption(n, stub, decryption_time)
    oracle_type = ""
    min_bad = min(rsps[msg_02_modif])
    max_bad = max(rsps[msg_02_modif])
    margin = max((max_bad - min_bad) / 2, minimum_delta)
    min_bad -= margin
    max_bad += margin
    small_good_msgs, large_good_msgs = observable_signals(
        rsps[msg_ok], min_bad, max_bad
    )
    if len(small_good_msgs) > 0 or len(large_good_msgs) > 0:
        new_small, new_large = observable_signals(rsps[msg_null_msg], min_bad, max_bad)
        if len(new_small) > 0 or len(new_large) > 0:
            oracle_type += "T"
            small_good_msgs = small_good_msgs.union(new_small)
            large_good_msgs = large_good_msgs.union(new_large)
        else:
            oracle_type += "F"

        new_small, new_large = observable_signals(rsps[msg_short_pad], min_bad, max_bad)
        if len(new_small) > 0 or len(new_large) > 0:
            oracle_type += "T"
            small_good_msgs = small_good_msgs.union(new_small)
            large_good_msgs = large_good_msgs.union(new_large)
        else:
            oracle_type += "F"

        oracle_type += "T"
        low_value, high_value = None, None
        if len(small_good_msgs) > 0:
            low_value = (max(small_good_msgs) + min_bad) / 2
        if len(large_good_msgs) > 0:
            high_value = (min(large_good_msgs) + max_bad) / 2
        return oracle_type, low_value, high_value
    return "Unknown", [], []


def evaluate_oracle(oracle: WOracle, n: int = 10, msg_len: Optional[int] = None):
    answers = [0] * 5
    for _i in range(n):
        plaintext_raw_msg = random_msg(oracle, msg_len=msg_len)
        correct_message = produce_altered_message(
            lambda x: x, oracle, plaintext_raw_msg
        )
        first_byte_modified = produce_altered_message(
            lambda x: change_byte(0, 1, x), oracle, plaintext_raw_msg
        )
        message02_modified = produce_altered_message(
            change_second_byte, oracle, plaintext_raw_msg
        )
        msg_pad_inf_8 = produce_altered_message(
            shorten_padding, oracle, plaintext_raw_msg
        )
        no_message = produce_altered_message(
            extend_padding_to_the_end, oracle, plaintext_raw_msg
        )
        if oracle.is_pkcs1_v15_conformant(correct_message):
            answers[0] += 1
        if oracle.is_pkcs1_v15_conformant(first_byte_modified):
            answers[1] += 1
        if oracle.is_pkcs1_v15_conformant(message02_modified):
            answers[2] += 1
        if oracle.is_pkcs1_v15_conformant(msg_pad_inf_8):
            answers[3] += 1
        if oracle.is_pkcs1_v15_conformant(no_message):
            answers[4] += 1

    return answers
