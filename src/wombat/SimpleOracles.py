"""The SimpleOracles modules defines trivial pure oracles allowing to
test different attack algorithms written in the Wombat framework.

It also contains helpers to produce a WOracle object from a WStub
object, once the relevant behaviours have been identified. The
produced oracle then logically transforms the behaviours obtained by
decrypt into True or False verdict for oracle."""


import time
from typing import List, Any
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend

from wombat.WTypes import WStub, WOracle, VirtualFunctionCalled


class PureOracle(WOracle):
    def __init__(self, keysize: int = 1024):
        WOracle.__init__(self)
        if keysize is not None:
            self.private_key_object = rsa.generate_private_key(
                65537, keysize, default_backend()
            )
        self.challenge_plaintext = b"Hello world!"

    def oracle(self, c: bytes):
        d = self.private_key_object.private_numbers().d
        plaintext_int = pow(self.int_of_bytes(c), d, self.n())
        plaintext = self.bytes_of_int(plaintext_int)
        return self.call(plaintext)

    def call(self, _plaintext: bytes) -> bool:
        raise VirtualFunctionCalled("Unknown Oracle type.")


# pylint: disable=invalid-name
class TTT_Oracle(PureOracle):
    def call(self, plaintext: bytes) -> bool:
        return plaintext[:2] == b"\x00\x02"


# pylint: disable=invalid-name
class TFT_Oracle(PureOracle):
    def call(self, plaintext: bytes) -> bool:
        return plaintext[:2] == b"\x00\x02" and b"\x00" not in plaintext[2:10]


# pylint: disable=invalid-name
class FTT_Oracle(PureOracle):
    def call(self, plaintext: bytes) -> bool:
        return plaintext[:2] == b"\x00\x02" and b"\x00" in plaintext[2:]


# pylint: disable=invalid-name
class FFT_Oracle(PureOracle):
    def call(self, plaintext: bytes) -> bool:
        return (
            plaintext[:2] == b"\x00\x02"
            and b"\x00" not in plaintext[2:10]
            and b"\x00" in plaintext[10:]
        )


# pylint: disable=invalid-name
class FFF_Oracle(PureOracle):
    def __init__(self, expected_len: int, keysize: int = 1024):
        PureOracle.__init__(self, keysize=keysize)
        self.expected_len = expected_len
        self.challenge_plaintext = b"A" * self.expected_len

    def call(self, plaintext: bytes) -> bool:
        return (
            plaintext[:2] == b"\x00\x02"
            and b"\x00" not in plaintext[2 : -(self.expected_len + 1)]
            and plaintext[-(self.expected_len + 1)] == 0
        )


class ExplicitOracleBehaviour:
    def __init__(self, good, bad):
        self.good = good
        self.bad = bad

    def good_behaviour(self):
        return self.good

    def bad_behaviour(self):
        return self.bad


class TimingOracleBehaviour:
    def __init__(self, good_time, bad_time):
        self.good_time = good_time
        self.bad_time = bad_time

    def good_behaviour(self):
        time.sleep(self.good_time)
        return b""

    def bad_behaviour(self):
        time.sleep(self.bad_time)
        return b""


class UnknownBehaviour(Exception):
    pass


class OracleFromStub(WOracle):
    def __init__(
        self, stub: WStub, good_observations: List[Any], bad_observations: List[Any]
    ):
        WOracle.__init__(self)
        self.stub = stub
        self.challenge = stub.get_challenge()
        self.good_observations = good_observations
        self.bad_observations = bad_observations
        self.treat_unknown_behaviour_as_false_signal = False

    def accept_unknown_behaviour(self):
        self.treat_unknown_behaviour_as_false_signal = True

    def get_public_key(self):
        return self.stub.get_public_key()

    def oracle(self, c: bytes) -> bool:
        try:
            result = self.stub.decrypt(c)
        except Exception as e:
            if self.treat_unknown_behaviour_as_false_signal:
                return False
            raise UnknownBehaviour(f"Exception {e}") from e

        if result in self.good_observations:
            return True
        if result in self.bad_observations:
            return False
        if self.treat_unknown_behaviour_as_false_signal:
            return False
        raise UnknownBehaviour(str(result))


# Use this OracleFromStub if stub.decrypt is not constant, and is
# slightly different from the good_observations
class OracleFromStubWithVariableTrueSignals(OracleFromStub):
    def __init__(
        self,
        stub: WStub,
        pattern_in_true_signals: List[bytes],
        pattern_in_false_signals: List[bytes],
    ):
        OracleFromStub.__init__(self, stub, [], [])
        self.pattern_in_true_signals = pattern_in_true_signals
        self.pattern_in_false_signals = pattern_in_false_signals
        self.counter = 0

    def oracle(self, c: bytes) -> bool:
        self.counter += 1
        if self.counter % 1000 == 0:
            print(self.counter)
        try:
            result = self.stub.decrypt(c)
        except Exception as e:
            if self.treat_unknown_behaviour_as_false_signal:
                return False
            raise UnknownBehaviour(f"Exception {e}") from e

        for pattern in self.pattern_in_true_signals:
            if result.find(pattern) != -1:
                return True
        for pattern in self.pattern_in_false_signals:
            if result.find(pattern) != -1:
                return False

        if self.treat_unknown_behaviour_as_false_signal:
            return False
        raise UnknownBehaviour(str(result))


class StubFromOracle(WStub):
    def __init__(self, oracle: WOracle, behaviour):
        WStub.__init__(self)
        self.public_key_object = oracle.get_public_key()
        self.oracle = oracle
        self.behaviour = behaviour
        self.challenge = oracle.get_challenge()

    def do_decrypt(self, c: bytes):
        if self.oracle.is_pkcs1_v15_conformant(c):
            return self.behaviour.good_behaviour()
        return self.behaviour.bad_behaviour()
