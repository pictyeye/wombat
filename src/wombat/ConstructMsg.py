"""ConstructMsg contains various helpers to manipulate cleartext and
encrypted messages before submitting them to a stub or an oracle."""

from typing import Callable, Optional
from random import randrange
import codecs
from wombat.WTypes import WPubKeyHolder, WOracle, InputIsTooBig


def _apply_f_and_encrypt(
    f: Callable[[bytes], bytes],
    pubkey_holder: WPubKeyHolder,
    formated_plaintext: bytes,
) -> bytes:
    """Internal function. Alter the given plaintext by applying a
    function, then encrypt the result."""

    altered_plaintext = f(formated_plaintext)
    altered_plaintext_int = pubkey_holder.int_of_bytes(altered_plaintext)
    altered_ciphertext_int = pow(
        altered_plaintext_int, pubkey_holder.e(), pubkey_holder.n()
    )
    altered_ciphertext = pubkey_holder.bytes_of_int(altered_ciphertext_int)
    return altered_ciphertext


def produce_altered_message(
    f: Callable[[bytes], bytes], pubkey_holder: WPubKeyHolder, cleartext: bytes
) -> bytes:
    """This function formats a message using PKCS#1 v1.5 type 2 padding,
    then alters the formated_plaintext using the given function, and
    apply raw RSA encryption."""

    n = pubkey_holder.nlen()
    if n < len(cleartext) + 11:
        raise InputIsTooBig
    formated_prefix = bytearray()
    formated_prefix.append(0)
    formated_prefix.append(2)
    for _i in range(n - len(cleartext) - 3):
        formated_prefix.append(randrange(1, 256))
    formated_prefix.append(0)
    formated_plaintext = bytes(formated_prefix) + cleartext

    return _apply_f_and_encrypt(f, pubkey_holder, formated_plaintext)


def alter_challenge(f: Callable[[bytes], bytes], oracle: WOracle) -> bytes:
    """This helper produced an altered version of the challenge by
    applying a function to the formatted plaintext."""

    c = oracle.get_challenge()
    plaintext = oracle.raw_decrypt(c)
    return _apply_f_and_encrypt(f, oracle, plaintext)


def change_byte(pos: int, val: int, msg: bytes) -> bytes:
    """Replace a given byte by a given value in a message.

    This helper can be used with produce_altered_message or
    alter_challenge."""

    result = bytearray(msg)
    result[pos] = val
    return bytes(result)


def change_second_byte(msg: bytes) -> bytes:
    """Change the second byte with an invalid value (different from 2) in
    a message.

    This helper can be used with produce_altered_message or
    alter_challenge."""

    return change_byte(1, 2 ^ randrange(1, 256), msg)


def shorten_padding(msg: bytes) -> bytes:
    """Add a null byte in the 10 first bytes of the message. This has the
    effect to produce a message with a padding too short.

    This helper can be used with produce_altered_message or
    alter_challenge."""

    return change_byte(randrange(2, 10), 0, msg)


def change_msg_len(msg: bytes, new_len: int) -> bytes:
    """Change a formatted message to ensure the length of the encapsulated
    message correspond to the expected new_len.

    This helper can be used with produce_altered_message or
    alter_challenge."""

    offset = msg.find(b"\x00", 2)
    if offset < 0:
        old_len = 0
    else:
        old_len = len(msg) - offset - 1
    print(offset, old_len)
    result = bytearray(msg)
    if old_len == new_len:
        return msg
    if old_len > new_len:
        result[-(old_len + 1)] = 0xFF
    result[-(new_len + 1)] = 0x00
    return bytes(result)


def extend_padding_to_the_end(msg: bytes) -> bytes:
    """Remove null bytes from the message after the two first bytes."""

    return msg[:2] + msg[2:].replace(b"\x00", b"\x01")


def random_msg(pubkey_holder: WPubKeyHolder, msg_len: Optional[int] = None) -> bytes:
    """Produce a random message of the given length. If no length is
    given, the length will be a random value compatible with the given
    oracle."""

    if msg_len is None:
        max_len = pubkey_holder.nlen() - 11
        msg_len = randrange(1, max_len)
    msg = b""
    for _i in range(msg_len):
        rnd = randrange(256)
        msg += b"%2.2x" % rnd
    return codecs.decode(msg, "hex")
