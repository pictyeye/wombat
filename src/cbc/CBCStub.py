from wombat.WTypes import WStub
from cbc import AESImplementation, TripleDesImplementation


class AesCbcStub(WStub):
    """CBCStub for our own AES-CBC decryption implementation."""

    def __init__(self, key):
        WStub.__init__(self)
        self.symmetric_key = key

    def encrypt(self, m):
        """This is not RSA here, so we override this method.
        AES encryption with the symmetric key defined in the object. CBC mode is used"""
        return AESImplementation.aes_encryption(m, self.symmetric_key)

    def get_challenge(self):
        """Challenge encrypted with our AES encryption implementation. CBC mode is used."""
        message = b"Here is the challenge to recover !"
        return self.encrypt(message)

    def do_decrypt(self, c):
        try:
            AESImplementation.aes_decryption(c, self.symmetric_key)
            return True
        # pylint: disable=broad-except
        except Exception:
            return False


class TripleDesCbcStub(WStub):
    """CBCStub for our own AES-CBC decryption implementation."""

    def __init__(self, key):
        WStub.__init__(self)
        self.symmetric_key = key

    def encrypt(self, m):
        """This is not RSA here, so we override this method.
        3DES encryption with the symmetric key defined in the object. CBC mode is used"""
        return TripleDesImplementation.triple_des_encryption(m, self.symmetric_key)

    def get_challenge(self):
        """Challenge encrypted with our 3DES encryption implementation. CBC mode is used."""
        message = b"Here is the challenge to recover !"
        return self.encrypt(message)

    def do_decrypt(self, c):
        try:
            TripleDesImplementation.triple_des_decryption(c, self.symmetric_key)
            return True
        # pylint: disable=broad-except
        except Exception:
            return False
