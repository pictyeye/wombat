import os
import pytest
from cryptography.hazmat.primitives import padding
from wombat.SimpleOracles import OracleFromStub
from cbc import CBCStub, DecryptionAttack


@pytest.fixture
def aes_key():
    return os.urandom(32)


@pytest.fixture
def triple_des_key():
    return os.urandom(24)


def unpad(msg, size_block):
    """unpad the message. PKCS7 padding standard."""
    unpadder = padding.PKCS7(8 * size_block).unpadder()
    decoded_msg = unpadder.update(msg) + unpadder.finalize()
    return decoded_msg


def test_with_our_own_aes_decryptor(aes_key):
    """Test the attack on the Oracle obtained from our AES-CBC implementation Stub"""
    stub = CBCStub.AesCbcStub(aes_key)
    challenge = stub.get_challenge()
    oracle = OracleFromStub(stub, {True}, {False})
    attack = DecryptionAttack.DecryptionAttack(oracle, 16)
    recovered_msg = attack.run(challenge)
    recovered_msg = unpad(recovered_msg, 16)
    assert recovered_msg == b"Here is the challenge to recover !"


def test_with_our_own_3des_decryptor(triple_des_key):
    """Test the attack on the Oracle obtained from our 3DES-CBC implementation Stub"""
    stub = CBCStub.TripleDesCbcStub(triple_des_key)
    challenge = stub.get_challenge()
    oracle = OracleFromStub(stub, {True}, {False})
    attack = DecryptionAttack.DecryptionAttack(oracle, 8)
    recovered_msg = attack.run(challenge)
    recovered_msg = unpad(recovered_msg, 8)
    assert recovered_msg == b"Here is the challenge to recover !"
