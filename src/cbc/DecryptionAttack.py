from binascii import unhexlify, hexlify


class DecryptionAttack:
    def __init__(self, oracle, size_block):
        self.oracle = oracle
        self.size_block = size_block

    def split_cipher_text_in_blocks(self, c):
        """Split the cipher message into a list of blocks of adequate size.
        "ciphered_message" -> [ciphered, _message]"""
        return [c[i : i + self.size_block] for i in range(0, len(c), self.size_block)]

    def block_search_byte(self, i, pos, l):
        """Create custom block for the byte we search"""
        hex_char = hex(pos).split("0x")[1]
        return (
            "00" * (self.size_block - (i + 1))
            + ("0" if len(hex_char) % 2 != 0 else "")
            + hex_char
            + "".join(l)
        )

    def block_padding(self, i):
        """Create custom block for the padding"""
        l = []
        for _t in range(0, i + 1):
            l.append(
                ("0" if len(hex(i + 1).split("0x")[1]) % 2 != 0 else "")
                + (hex(i + 1).split("0x")[1])
            )
        return "00" * (self.size_block - (i + 1)) + "".join(l)

    # pylint: disable=too-many-arguments
    def create_block_for_oracle(
        self, current_block, previous_block, tested_byte, index_tested_byte, valid_bytes
    ):
        """Create altered block to send to the oracle"""
        bp = self.block_padding(index_tested_byte)
        assert len(bp) == self.size_block * 2
        bsb = self.block_search_byte(index_tested_byte, tested_byte, valid_bytes)
        assert len(bsb) == self.size_block * 2
        altered_block = hex(int(previous_block, 16) ^ int(bsb, 16) ^ int(bp, 16)).split(
            "0x"
        )[1]
        altered_block = bytes(altered_block, "utf-8")
        # hex doesn't add the 0 at the beginning, we need to add them
        # to keep an altered_block of size size_block
        while len(altered_block) < self.size_block * 2:
            altered_block = b"0" + altered_block
        cipher_for_oracle = altered_block + current_block
        cipher_for_oracle = bytes.fromhex(str(cipher_for_oracle, "utf-8"))
        return cipher_for_oracle

    # pylint: disable=too-many-nested-blocks
    def run(self, encoded_data):
        """Run the attack"""
        c = self.split_cipher_text_in_blocks(encoded_data)
        c = list(map(hexlify, c))

        hex_recovered_message = ""
        done_treating_padding = False

        for i in reversed(range(1, len(c))):
            current_block = c[i]
            previous_block = c[i - 1]
            assert len(c[i]) == len(c[i - 1]) == self.size_block * 2
            valid_bytes = ""

            for j in range(self.size_block):
                for b in range(256):
                    # we don't want bp ^ bsb = 0
                    if b != j + 1:
                        cipher_for_oracle = self.create_block_for_oracle(
                            current_block,
                            previous_block,
                            b,
                            j,
                            valid_bytes,
                        )
                        response_oracle = self.oracle.oracle(cipher_for_oracle)
                        if response_oracle:
                            valid_bytes = hex(b).split("0x")[1] + valid_bytes
                            if len(valid_bytes) % 2 != 0:
                                valid_bytes = "0" + valid_bytes
                            break
                    # true padding is 01
                    if b > self.size_block - 1 and j == 0 and not done_treating_padding:
                        cipher_for_oracle = self.create_block_for_oracle(
                            current_block,
                            previous_block,
                            1,
                            j,
                            valid_bytes,
                        )
                        response_oracle = self.oracle.oracle(cipher_for_oracle)
                        if response_oracle:
                            done_treating_padding = True
                            valid_bytes = hex(1).split("0x")[1] + valid_bytes
                            if len(valid_bytes) % 2 != 0:
                                valid_bytes = "0" + valid_bytes
                            break
                    # case where b = j+1 is the good byte (when treating padding or not)
                    if (
                        b > self.size_block - 1 and not done_treating_padding
                    ) or b == 255:
                        cipher_for_oracle = self.create_block_for_oracle(
                            current_block,
                            previous_block,
                            j + 1,
                            j,
                            valid_bytes,
                        )
                        response_oracle = self.oracle.oracle(cipher_for_oracle)
                        if response_oracle:
                            done_treating_padding = True
                            valid_bytes = hex(j + 1).split("0x")[1] + valid_bytes
                            if len(valid_bytes) % 2 != 0:
                                valid_bytes = "0" + valid_bytes
                            break

            hex_recovered_message = valid_bytes + hex_recovered_message

        return unhexlify(hex_recovered_message)
