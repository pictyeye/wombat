import os
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import padding


def unpad(msg, size_block):
    """unpad the message. PKCS7 padding standard.
    Note : CBC padding attack may not work on another padding standard."""
    unpadder = padding.PKCS7(8 * size_block).unpadder()
    decoded_msg = unpadder.update(msg) + unpadder.finalize()
    return decoded_msg


def triple_des_encryption(data, key):
    """3DES encryption. Key must be 24 bytes. IV is 8 bytes.
    PKCS7 padding standard : "Hello World" -> "Hello world|05|05|05|05|05"."""
    iv = os.urandom(8)
    # padding
    padder = padding.PKCS7(64).padder()
    data = padder.update(data) + padder.finalize()
    # encrypting
    cipher = Cipher(algorithms.TripleDES(key), modes.CBC(iv), default_backend())
    encryptor = cipher.encryptor()
    encoded_data = encryptor.update(data) + encryptor.finalize()
    return iv + encoded_data


def triple_des_decryption(ciphered_msg, symmetric_key):
    """3DES decryption"""
    iv = ciphered_msg[:8]
    true_ciphered_msg = ciphered_msg[8:]
    cipher = Cipher(
        algorithms.TripleDES(symmetric_key), modes.CBC(iv), backend=default_backend()
    )
    decrypter = cipher.decryptor()
    decoded_msg = decrypter.update(true_ciphered_msg) + decrypter.finalize()
    # removing extra padding
    decoded_msg = unpad(decoded_msg, 8)
    return decoded_msg
